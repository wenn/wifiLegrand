    <?php
      include('include/inc_header_home.php');
    ?>
    <link type="text/css" rel="stylesheet" href="css/settings.css">
  </head>

  <body>
    <div class="forbidLandscape hidden-sm hidden-lg hidden-md hidden"> <!-- Start forbidLandscape -->
      <div class="forbidLandscapeTable">
        <div class="forbidLandscapeTableCell">
          <p class="forbidLandscapeTableCellParaph text-center">Veuillez utiliser votre smartphone en mode portrait</p>
        </div>
      </div>
    </div> <!-- /forbidLandscape -->
    <form id="ipForm" data-toggle="validator" role="form" action="">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
              <div class="col-lg-9 col-md-9 col-sm-9 hidden-xs">
                <h1 class="font-title settings"><!--{$home_title}--><?= _('Réglages'); ?></h1>
              </div>
               <div class="hidden-lg hidden-md hidden-sm col-xs-4">
                <h1 class="font-title settings majuscule"><!--{$home_title}--><?= _('Réglages'); ?></h1>
              </div>
              <div class="col-lg-offset-0 col-lg-2 col-md-offset-0 col-md-2 col-sm-offset-1 col-sm-2 hidden-xs">
                <!-- Select language -->
                <select class="flag-option font-title-select" id="langs">
                  <option value="fr" id="fr">Français</option>
                  <option value="en" id="en">English</option>
                  <option value="it" id="it">Italiano</option>
                  <option value="es" id="es">Español</option>
                  <option value="pt" id="pt">Português</option>
                  <option value="ru" id="ru">Русский</option>
                </select>
                <div class="alert alert-danger hidden" id="langError" role="alert"></div>
              </div>
              <div class="col-xs-offset-4 col-xs-3 hidden-lg hidden-md hidden-sm margintop">
                <!-- Select language -->
                <img class="img-responsive paddingtop minWidthLogo" src="images/legrand_logo.png" alt="Logo Legrand"> <!-- class minWidthLogo pour le responsive mobile -->
              </div>
            </div>
            <!-- Colonne gauche-->
            <div class="container">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> <!-- partage de la colonne lg-12 md-12 en 2 -->
                  <div class="row">
                    <h2 class="font-title settings"><?= _('General'); ?></h2>
                    <div class="row">
                      <div class="col-lg-6 col-md-5 col-sm-8 col-xs-5">
                        <label class="font-title-settings"> <?= _('Nom du point d\'accès'); ?></label>
                      </div>
                      <div class="col-lg-4 col-md-5 col-sm-4 col-xs-7">
                        <input type="text" name="access_point"  class="custom-input font-title-settings-input custom-input-settings-bar right" value="<?= $jsonData['Device_name']; ?>" tabindex="6">
                      </div>
                      <div class="col-lg-offset-5 col-lg-5 col-md-offset-4 col-md-6 col-sm-offset-6 col-sm-6 hidden-xs">
                        <p class="font-settings font-title-input right">
                          <small><?= _('Actif depuis le ' . $date); ?></small>
                        </p>
                      </div>
                      <!-- Version mobile pour la section "Actif depuis le 20 janvier 2017" -->
                      <div class="col-xs-12 hidden-lg hidden-md hidden-sm">
                        <br><br>
                        <p class="font-settings"><small><?= _('Actif depuis le 20 janv 2017'); ?></small></p>
                      </div>
                      <!-- Fin version mobile -->
                      <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div class="clearfix content-settings-item"></div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <h2 class="font-title settings">Wifi</h2>
                    <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-7 col-xs-7">
                        <label for="reglementation" class="font-title-settings">
                        <?= _('Domaine de réglementation'); ?>
                          <a href="#rules" data-toggle="tab" title="Lorem ipsum dolor sit amet, consetetur sadipscing">&nbsp;
                            <img src="./images/help_icon.png" alt="icon info">
                          </a>
                        </label>
                      </div>
                      <div class="col-lg-offset-2 col-lg-2 col-md-offset-2 col-md-2 col-sm-offset-3 col-sm-2 col-xs-offset-2 col-xs-3">
                        <!-- Select language -->
                        <select name="domain" class="form-control select-regl custom-select-settings font-title-select" id="regl">
                          <option value="fr" id="fr" selected>Fr</option>
                          <option value="en" id="en">En</option>
                          <option value="it" id="it">It</option>
                          <option value="es" id="es">Es</option>
                          <option value="pt" id="pt">Po</option>
                          <option value="ru" id="ru">Ru</option>
                        </select>
                        <div class="alert alert-danger hidden" id="langError" role="alert"></div>
                      </div>
                      <div class="clear"></div>
                      <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6">
                        <label class="font-title-settings"><?= _('Canal'); ?></label>
                      </div>
                      <div class="col-lg-offset-3 col-lg-2  col-md-offset-3 col-md-2 col-sm-offset-4 col-sm-2 col-xs-offset-3 col-xs-3">
                        <select class="form-control selectCanal custom-select-settings font-title-select" name="canalChoice" id="canalChoice">
                          <option value="1" id="c1">1</option>
                          <option value="2" id="c2">2</option>
                          <option value="3" id="c3">3</option>
                          <option value="4" id="c4">4</option>
                          <option value="5" id="c5">5</option>
                          <option value="6" id="c6">6</option>
                          <option value="7" id="c7">7</option>
                          <option value="8" id="c8">8</option>
                          <option value="9" id="c9">9</option>
                          <option value="10" id="c10">10</option>
                          <option value="11" id="c11">11</option>
                          <option value="12" id="c12" selected>12</option>
                        </select>
                        <div class="alert alert-danger hidden" id="CanalError" role="alert"></div>
                        <span class="form-control-feedback"></span>
                      </div>
                      <div class="clear"></div>
                      <div class="col-lg-6 col-md-5 col-sm-6 col-xs-5">
                        <label class="font-title-settings"><?= _('Nom du réseau (SSID)'); ?></label>
                      </div>
                      <div class="col-lg-offset-0 col-lg-4 col-md-offset-0 col-md-5 col-sm-offset-2 col-sm-4 col-xs-offset-0 col-xs-7">
                        <input type="text" name="SSID"  class="custom-input font-title-input custom-input-settings-bar right" value="<?= $jsonData['SSID']; ?>" tabindex="6">
                      </div>
                      <div class="clear"></div>
                      <div class="col-lg-5 col-md-3 col-sm-6 col-xs-2">
                        <label for="cryptage" class="font-title-settings heightlabel"><?= _('Cryptage'); ?></label>
                      </div>
                     <div class="col-lg-offset-1 col-lg-4 col-md-offset-2 col-md-5 col-sm-offset-3 col-sm-3 col-xs-offset-3 col-xs-7">
                 <select class="form-control custom-select-settings font-title-select" name="cryptChoice" id="cryptChoice">
                 <!-- &#160 permet d'uniformiser les espaces entre le curseur du select et le titre -->
                   <option value="Open" id="cry1" selected>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Open</option>
                    <option value="wep-psk" id="cry2">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;wep-psk</option>
                    <option value="wpa-psk" id="cry3">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;wpa-psk</option>
                    <option value="802.11i-psk" id="cry4">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;802.11i-psk</option>
                    <option value="wpa-or-802.11i-psk" id="cry5">wpa-or-802.11i-psk</option>
                 </select>
              </div>
                      <div class="clear"></div>
                      <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                        <label for="cleWifi" class="font-title-settings"><?= _('Clé WiFi'); ?></label>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <input type="text" id="WiFiKey" name="WiFiKey" class="custom-input font-title-input custom-input-settings-bar right" value="****************" tabindex="6">
                      </div>
                      <div class="clear"></div>
                      <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div class="clearfix content-settings-item"></div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="row">
                      <div class="col-lg-7 col-md-7 col-sm-6 col-xs-4">
                        <h2 class="font-title settings">WPS</h2>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-8">

                        <input type="checkbox" id="switch" <?php if(isset($jsonData['WpsStatus']) && $jsonData['WpsStatus'] === 'ON'){echo $checked;} ?>>
                        <label class="checkSwitch" for="switch"></label>
                        <span id="alertWps" class="alert fade"></span>
                      </div>
                      <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div class="clearfix content-settings-item"></div>
                      </div> <!-- Fin separateur et clear -->
                    </div> <!-- Fin 1er row -->
                  </div>
                  <div class="row">
                    <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <h2 class="font-title protocols">Announcing protocols configuration</h2>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                        <span id="alertProtocol" class="alert fade"></span>
                        <input type="checkbox" id="swatch" <?php if(isset($jsonData['ProtocolStatus']) && $jsonData['ProtocolStatus'] === 'ON'){echo $checked;} ?>>
                        <label class="checkSwitch" for="swatch"></label>
                      </div>
                      <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div class="clearfix content-settings-item"></div>
                      </div>
                    </div>
                  </div>
                </div> <!-- Fin colonne gauche col-lg-6 col-md-6 -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> <!-- Colonne droite -->
                    <div class="row">
                    <div class="row">
                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h2 class="font-title settings">Firmware</h2>
                      </div>
                      <div class="col-lg-7 col-md-7 col-sm-9 col-xs-9">
                        <div class="f1-buttons margintop">
                          <input type="button" class="check-launch btn btn-launch" value="Check for update">
                          <input type="button" class="check-launch btn btn-check" value="Launch update">
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                        <label for="version" class="font-title-settings">Version</label>
                      </div>
                      <div class="col-lg-5 col-md-5 col-sm-9 col-xs-6">
                        <input type="text" name="version" class="custom-input font-title-input right" value="1.2" tabindex="8">
                      </div>
                      <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div class="clearfix content-settings-item"></div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-5 col-xs-6">
                        <h2 class="font-title settings">IP Configuration</h2>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-7 col-xs-6">
                        <div class="right margestop">
                          <div class="btn-group" id="change" data-toggle="buttons">
                            <label id="LabelAutoIP" for="AutoIP" class="btn btn-default btn-on btn-md active">
                              <input type="radio" id="AutoIP" value="AutoIP" name="inputIPtype">Auto
                            </label>
                            <label for="StaticIP" class="btn btn-default btn-off btn-md">
                              <input id="LabelStaticIP" type="radio"  id="StaticIP" value="StaticIP" name="inputIPtype">Manual
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="row">
                      <div class="col-lg-3 col-md-2 col-sm-4 col-xs-1">
                        <label class="font-title-settings"><?= ('Adresse&nbsp;IP'); ?></label>
                      </div>
                      <div id="content2" style="display: none;">
                        <div class="col-lg-offset-0 col-lg-7 col-md-offset-0 col-md-8 col-sm-offset-0 col-sm-8 col-xs-offset-1 col-xs-9">
                          <span class="right">
                            <input type="text" class="form-control custom-input-settings no-bottomBar right" name="ip_addr" maxlength="12" value="AP IP-ADDRESS" readonly>
                          </span>
                        </div>
                      <div class="clear"></div>
                           </div>
                      <div id="content1">
                        <div class="col-lg-7 col-md-8 col-sm-7 col-xs-9 right">
                          <span>
                           <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="ip_addr_ipv4_1" id="ip_addr_ipv4_1" maxlength="3" value="" placeholder="XXX">
                            </span>.&nbsp;
                          <span>
                            <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="ip_addr_ipv4_2" id="ip_addr_ipv4_2" maxlength="3" value="" placeholder="XXX">
                          </span>.&nbsp;
                          <span>
                            <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="ip_addr_ipv4_3" id="ip_addr_ipv4_3" maxlength="3" value="" placeholder="XXX">
                          </span>.&nbsp;
                          <span>
                            <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="ip_addr_ipv4_4" id="ip_addr_ipv4_4" maxlength="3" value="" placeholder="XXX">
                          </span>/
                          <span>
                            <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="ip_addr_prefix" id="ip_addr_prefix" maxlength="2" value="" placeholder="XX">
                          </span>
                        </div>
                        <!-- Colonne pour l'affichage des coches -->
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                           <span id="glyph1-ko" class="glyphicon glyphicon-remove" style='display:none'></span>
                           <span id="glyph1-ok" class="glyphicon glyphicon-ok" style='display:none'></span>
                        </div><!-- Fin colonne pour l'affichage des coches -->

                        <div class="clear"></div>

                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-1">
                        <label for="subnet" class="font-title-settings">Subnet&nbsp;mask</label>
                        </div>

                        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-9 right">
                          <span>
                            <input type="text" class="form-control ip-form input-placeholder text-center" name="netmask_ipv4_1" id="netmask_ipv4_1" size="3" maxlength="3" value="" placeholder="XXX">
                          </span>&nbsp;.&nbsp;
                          <span>
                            <input type="text" class="form-control ip-form input-placeholder text-center" name="netmask_ipv4_2" id="netmask_ipv4_2" size="3" maxlength="3" value="" placeholder="XXX">
                          </span>&nbsp;.&nbsp;
                          <span>
                            <input type="text" class="form-control ip-form input-placeholder text-center" name="netmask_ipv4_3" id="netmask_ipv4_3" size="3" maxlength="3" value="" placeholder="XXX">
                          </span>&nbsp;.&nbsp;
                          <span>
                            <input type="text" class="form-control ip-form input-placeholder text-center" name="netmask_ipv4_4" id="netmask_ipv4_4" size="3" maxlength="3" value="" placeholder="XXX">
                          </span>
                        </div>

                         <!-- Colonne pour l'affichage des coches -->
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                           <span id="glyph2-ko" class="glyphicon glyphicon-remove" style='display:none'></span>
                           <span id="glyph2-ok" class="glyphicon glyphicon-ok" style='display:none'></span>
                        </div><!-- Fin colonne pour l'affichage des coches -->

                        <div class="clear"></div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-1">
                        <label for="gateway" class="font-title-settings">Default&nbsp;gateway</label>
                        </div>

                        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-9 right">
                          <span>
                            <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="gateway_ipv4_1" id="gateway_ipv4_1" size="3" maxlength="3" value="" placeholder="XXX">
                          </span>&nbsp;.&nbsp;
                          <span>
                            <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="gateway_ipv4_2" id="gateway_ipv4_2" size="3" maxlength="3" value="" placeholder="XXX">
                          </span>&nbsp;.&nbsp;
                          <span>
                            <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="gateway_ipv4_3" id="gateway_ipv4_3" size="3" maxlength="3" value="" placeholder="XXX">
                          </span>&nbsp;.&nbsp;
                          <span>
                            <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="gateway_ipv4_4" id="gateway_ipv4_4" size="3" maxlength="3" value="" placeholder="XXX">
                          </span>
                        </div>
                          <!-- Colonne pour l'affichage des coches -->
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                           <span id="glyph3-ko" class="glyphicon glyphicon-remove" style='display:none'></span>
                           <span id="glyph3-ok" class="glyphicon glyphicon-ok" style='display:none'></span>
                        </div><!-- Fin colonne pour l'affichage des coches -->


                        <div class="clear"></div>

                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-1">
                        <label for="dns" class="font-title-settings">DNS</label>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-9 right">
                          <span>
                            <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="dns_1" id="dns_1" size="3" maxlength="3" value="" placeholder="XXX">
                          </span>&nbsp;.&nbsp;
                          <span>
                            <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="dns_2" id="dns_2" size="3" maxlength="3" value="" placeholder="XXX">
                          </span>&nbsp;.&nbsp;
                          <span>
                            <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="dns_3" id="dns_3" size="3" maxlength="3" value="" placeholder="XXX">
                          </span>&nbsp;.&nbsp;
                          <span>
                            <input onkeyup="verif(this)" type="text" class="form-control ip-form input-placeholder text-center" name="dns_4" id="dns_4" size="3" maxlength="3" value="" placeholder="XXX">
                          </span>
                        </div>
                         <!-- Colonne pour l'affichage des coches -->
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                           <span id="glyph4-ko" class="glyphicon glyphicon-remove" style='display:none'></span>
                           <span id="glyph4-ok" class="glyphicon glyphicon-ok" style='display:none'></span>
                        </div><!-- Fin colonne pour l'affichage des coches -->
                       <div class="clear"></div>
                      </div>

                      <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div class="clearfix content-settings-item"></div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="row">
                      <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6">
                        <h2 class="font-title settings"><?= _('Mot de passe'); ?></h2>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                        <label class="font-title-settings"><?= _('Ancien mot de passe admin'); ?></label>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <input type="password"  class="custom-input font-title-input custom-input-settings-bar right" id="inputPassword" name="inputOldPassword" tabindex="6" placeholder="********" value="">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                        <label class="font-title-settings"><?= _('Nouveau mot de passe admin'); ?></label>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <input type="password" class="custom-input font-title-input custom-input-settings-bar right" id="inputPassword" name="inputNewPassword" tabindex="6" placeholder="********" value="">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                        <label class="font-title-settings"><?= _('Répéter mot de passe admin'); ?></label>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <input type="password"  class="custom-input font-title-input custom-input-settings-bar right" id="inputPassword" name="inputNewPassword2" tabindex="6" placeholder="********" value="">
                      </div>
                      <div class="clear"></div>
                      <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div class="clearfix content-settings-item"></div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="row">
                      <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div class="f1-buttons pull-right margintop">
                          <input type="button" class="annul-save btn btn-annul" value="<?= _('Annuler'); ?>">
                          <input type="button" id="applyIP" class="annul-save btn btn-submit" value="<?= _('Enregistrer'); ?>">
                        </div>
                        <div class="text-right">
                          <span id="alert-config" class="alert fade"></span>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div> <!-- Fin colonne droite -->
              </div> <!-- Fin row -->
            </div> <!-- Fin container -->
          </div> <!-- row col-lg-6 -->
        </div> <!-- Col-lg-12 -->
        <div class="row footer"> <!-- Début row footer pour step 1 et pour version desktop -->
          <div class="margintop"></div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
              <div class="col-lg-2 col-md-2">
                <img class="img-responsive paddingtop" src="images/legrand_logo.png" alt="Logo Legrand">
              </div>
            </div>
          </div>
        </div>
      </div> <!-- fin container -->
    </form>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- <script src="js/validator.js"></script>-->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/jquery-cursor-functions.js"></script>
    <script src="js/ipValidation.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script>
      // switch buttons Auto/Manual
      $('#content2').show();
      $('#content1').hide();

      $("#change").click(function(){
        $("#content2").toggle();
        $("#content1").toggle();
      });
    </script>
    <script>
      // ToolTip Affichage de la bulle pour le domaine de réglementation
      $(function(){
        $('a[title]').tooltip();
      });
    </script>
    <script type="text/javascript">
      // Recovery Regulatory Domain in config file and attribute to #regl
      var domain = "<?php echo $jsonData['Regulatory_domain']; ?>";
      $("#regl option[value='" + domain + "']").attr('selected', 'selected');

      // Recovery canal in config file and attribute to #canalChoice
      var canal = "<?php if(isset($jsonData['Canal'])) echo $jsonData['Canal']; ?>";
      $("#canalChoice option[value='" + canal + "']").attr('selected', 'selected');

      // Recovery Encryption in config file and attribute to #cryptChoice
      var crypt = "<?php echo $jsonData['Encryption_type']; ?>";
      $("#cryptChoice option[value='" + crypt + "']").attr('selected', 'selected');

      // Recovery language in config file and attribute to #langs
      var lang = "<?php echo $jsonData['Language']; ?>";
      $("#langs option[value='" + lang + "']").attr('selected', 'selected');

      /**
       * Change Language
       **/
      $('#langs').on('change', function() {
        if(this.value != '') {
          $.ajax({
            url: "processing/i18n-home-cgi.php",
            type: "post",
            data: "lang=" + $("#langs option:selected").val(),
            dataType: 'json',
            success: function(json) {
              if (json.reponse === "ok") {
                location.reload(true);
                console.log(json.message);
              }
              else if (json.reponse === "error") {
                console.log(json.message);
              }
            },
            fail: function() {
              console.log("error on server");
            }
          });
        }
      });

      //WPS CheckBox
      $("#switch").change(function() {
        var status;
        if($('#switch').prop('checked')) {
          status = "ON";
        }
        else {
          status = "OFF";
        }
        $.ajax({
          url: "processing/wps-cgi.php",
          type: "post",
          data: "wpsStatus=" + status,
          dataType: 'json',
          success: function(json) {
            if (json.status === "ok") {
              if(json.wps_status === "ON") {
                $('#alertWps').text(json.message);
                $('#alertWps').addClass('alert-success');
                $('#alertWps').delay(800).addClass('in').show().fadeOut(300);
              }
              if(json.wps_status === "OFF") {
                $('#alertWps').text(json.message);
                $('#alertWps').addClass('alert-success');
                $('#alertWps').delay(800).addClass('in').show().fadeOut(300);
              }
              console.log(json.message);
            }
            else if (json.status === "error") {
              console.log(json.message);
              $('#alertWps').text(json.message);
              $('#alertWps').addClass('alert-danger');
              $('#alertWps').delay(5000).addClass('in').show().fadeOut(300);
            }
          },
          fail: function() {
            alert("error on server, temporarily unavailable");
            console.log("error on server");
          }
        });
      });

      //Annoucing Protocols CheckBox
      $("#swatch").change(function() {
        var status;
        if($('#swatch').prop('checked')) {
          status = "ON";
        }
        else {
          status = "OFF";
        }
        $.ajax({
          url: "processing/announcing-protocols-cgi.php",
          type: "post",
          data: "announcing_protocols_status=" + status,
          dataType: 'json',
          success: function(json) {
            if (json.status === "ok") {
              if(json.announcing_protocols_status === "ON") {
                $('#alertProtocol').text(json.message);
                $('#alertProtocol').addClass('alert-success');
                $('#alertProtocol').delay(800).addClass('in').show().fadeOut(300);
              }
              if(json.announcing_protocols_status === "OFF") {
                $('#alertProtocol').text(json.message);
                $('#alertProtocol').addClass('alert-success');
                $('#alertProtocol').delay(800).addClass('in').show().fadeOut(300);
              }
              console.log(json.message);
            }
            else if (json.status === "error") {
              console.log(json.message);
              $('#alertProtocol').text(json.message);
              $('#alertProtocol').addClass('alert-danger');
              $('#alertProtocol').delay(5000).addClass('in').show().fadeOut(300);
            }
          },
          fail: function() {
            alert("error on server, temporarily unavailable");
            console.log("error on server");
          }
        });
      });



      // //Firmware upgrade
      // $('#uploadfw').click(function() {
      //   $("#firmware_upgrade_error").addClass("hidden");
      //   var form = $('form')[0];
      //   var formData = new FormData(form);
      //   $.ajax({
      //     url: "firmware-cgi.php",
      //     type: "post",
      //     data: formData,
      //     dataType: 'json',
      //     processData: false,
      //     contentType: false,
      //     success: function(json) {
      //       if (json.status === "ok") {
      //         console.log("File uploaded");
      //         $("#fw_image").attr('disabled', true);
      //         $("#upload_firmware").submit();
      //       }
      //       else if (json.status === "error") {
      //         $("#firmware_upgrade_error").text(json.message).removeClass("hidden");
      //         console.log(json.message);
      //       }
      //       else {
      //         console.log(json.status + "\n" + json.message);
      //       }
      //     },
      //     fail: function() {
      //       alert("error on server");
      //       console.log("error on server");
      //     }
      //   });
      // });

      // On focus WiFiKey->val()=empty, on blur->val()='****************'
      $('#WiFiKey').on({
        focus: function() {
          var key = $('#WiFiKey').val();
          if (!key.match(/([A-Za-z0-9])\w+/)) {
            $(this).val('');
          }
        },
        blur: function() {
          if ($(this).val() == '') {
            $(this).val('****************');
          }
        }
      });

      function verif(chars) {
        // Caractères autorisés
        var regex = new RegExp("[0-9]", "i");
        var valid;
        for (x = 0; x < chars.value.length; x++) {
          valid = regex.test(chars.value.charAt(x));
          if (valid == false) {
            chars.value = chars.value.substr(0, x) + chars.value.substr(x + 1, chars.value.length - x + 1); x--;
          }
        }
      }

      // If Ip.keyCode = '.', go to next field
      $('#ip_addr_ipv4_1').on({
        keyup: function(event) {
          if (event.keyCode == 110) {
            $('#ip_addr_ipv4_2').focus();
          }
          if (event.shiftKey == true && event.keyCode == 59) {
            $('#ip_addr_ipv4_2').focus();
          }
        }
      });
      $('#ip_addr_ipv4_2').on({
        keyup: function(event) {
          if (event.keyCode == 110) {
            $('#ip_addr_ipv4_3').focus();
          }
          if (event.shiftKey == true && event.keyCode == 59) {
            $('#ip_addr_ipv4_3').focus();
          }
        }
      });
      $('#ip_addr_ipv4_3').on({
        keyup: function(event) {
          if (event.keyCode == 110) {
            $('#ip_addr_ipv4_4').focus();
          }
          if (event.shiftKey == true && event.keyCode == 59) {
            $('#ip_addr_ipv4_4').focus();
          }
        }
      });

      // If Gateway.val() = '.', go to next field
      $('#gateway_ipv4_1').on({
        keyup: function(event) {
          if (event.keyCode == 110) {
            $('#gateway_ipv4_2').focus();
          }
          if (event.shiftKey == true && event.keyCode == 59) {
            $('#gateway_ipv4_2').focus();
          }
        }
      });
      $('#gateway_ipv4_2').on({
        keyup: function(event) {
          if (event.keyCode == 110) {
            $('#gateway_ipv4_3').focus();
          }
          if (event.shiftKey == true && event.keyCode == 59) {
            $('#gateway_ipv4_3').focus();
          }
        }
      });
      $('#gateway_ipv4_3').on({
        keyup: function(event) {
          if (event.keyCode == 110) {
            $('#gateway_ipv4_4').focus();
          }
          if (event.shiftKey == true && event.keyCode == 59) {
            $('#gateway_ipv4_4').focus();
          }
        }
      });

      // If Dns.val() = '.', go to next field
      $('#dns_1').on({
        keyup: function(event) {
          if (event.keyCode == 110) {
            $('#dns_2').focus();
          }
          if (event.shiftKey == true && event.keyCode == 59) {
            $('#dns_2').focus();
          }
        }
      });
      $('#dns_2').on({
        keyup: function(event) {
          if (event.keyCode == 110) {
            $('#dns_3').focus();
          }
          if (event.shiftKey == true && event.keyCode == 59) {
            $('#dns_3').focus();
          }
        }
      });
      $('#dns_3').on({
        keyup: function(event) {
          if (event.keyCode == 110) {
            $('#dns_4').focus();
          }
          if (event.shiftKey == true && event.keyCode == 59) {
            $('#dns_4').focus();
          }
        }
      });

      // IP validation
      var validation = new ipValidation();
      $('#ip_addr_ipv4_1, #ip_addr_ipv4_2, #ip_addr_ipv4_3, #ip_addr_ipv4_4').on({
        keyup: function() {
          if ($('#ip_addr_ipv4_1').val() !== '' && $('#ip_addr_ipv4_2').val() !== '' && $('#ip_addr_ipv4_3').val() !== '' && $('#ip_addr_ipv4_4').val() !== '') {

            if (!validation.checkIPv4Byte1($('#ip_addr_ipv4_1')) || !validation.checkIPv4Byte23($('#ip_addr_ipv4_2')) || !validation.checkIPv4Byte23($('#ip_addr_ipv4_3')) || !validation.checkIPv4Byte4($('#ip_addr_ipv4_4')) || $('#ip_addr_ipv4_2').val() === '00' || $('#ip_addr_ipv4_2').val() === '000' || $('#ip_addr_ipv4_3').val() === '00' || $('#ip_addr_ipv4_3').val() === '000') {
              $('#glyph1-ok').hide();
              $('#glyph1-ko').show();
            }
            else {
              $('#glyph1-ko').hide();
              $('#glyph1-ok').show();
            }
          }
          else {
            $('#glyph1-ok').hide();
            $('#glyph1-ko').hide();
          }
          validateIPForm();
        }
      });

      /* CIDR2mask */
      $('#ip_addr_prefix').on({
        focus: function() {
          var netmaskFieldsValues = validation.CIDR2Mask($('#ip_addr_prefix'));
          $('#netmask_ipv4_1').val(netmaskFieldsValues.netmaskField1Value);
          $('#netmask_ipv4_2').val(netmaskFieldsValues.netmaskField2Value);
          $('#netmask_ipv4_3').val(netmaskFieldsValues.netmaskField3Value);
          $('#netmask_ipv4_4').val(netmaskFieldsValues.netmaskField4Value);
        },
        blur: function() {
          var netmaskFieldsValues = validation.CIDR2Mask($('#ip_addr_prefix'));
          $('#netmask_ipv4_1').val(netmaskFieldsValues.netmaskField1Value);
          $('#netmask_ipv4_2').val(netmaskFieldsValues.netmaskField2Value);
          $('#netmask_ipv4_3').val(netmaskFieldsValues.netmaskField3Value);
          $('#netmask_ipv4_4').val(netmaskFieldsValues.netmaskField4Value);
          validateIPForm();
        },
        keyup: function() {
          if ($('#ip_addr_prefix').val().length >= 1) {
            $('#netmask_ipv4_4').focus();
            $('#ip_addr_prefix').focus();
          }
        }
      });

      // Netmask Validation
      $('#netmask_ipv4_1, #netmask_ipv4_2, #netmask_ipv4_3, #netmask_ipv4_4, #ip_addr_prefix').on({
        blur: function() {
          var error = 0;
          if (!validation.checkIPv4NetmaskByte($('#netmask_ipv4_1'))) {
            error++;
          }

          if (!validation.checkIPv4NetmaskByteAndPrevious($('#netmask_ipv4_2'), $('#netmask_ipv4_1'))) {
            error++;
          }

          if (!validation.checkIPv4NetmaskByteAndPrevious($('#netmask_ipv4_3'), $('#netmask_ipv4_2'))) {
            error++;
          }

          if (!validation.checkIPv4NetmaskByteAndPrevious($('#netmask_ipv4_4'), $('#netmask_ipv4_3'))) {
            error++;
          }

          if (!validation.checkIPv4NetmaskByteAndPrevious($('#netmask_ipv4_2'), $('#netmask_ipv4_1'))) {
          }

          if (!validation.checkIPv4NetmaskByteAndPrevious($('#netmask_ipv4_3'), $('#netmask_ipv4_2'))) {
            error++;
          }

          if (!validation.checkIPv4NetmaskByteAndPrevious($('#netmask_ipv4_4'), $('#netmask_ipv4_3'))) {
            error++;
          }

          if (!validation.checkIPv4NetmaskByteAndPrevious($('#netmask_ipv4_3'), $('#netmask_ipv4_2'))) {
            error++;
          }

          if (!validation.checkIPv4NetmaskByteAndPrevious($('#netmask_ipv4_4'), $('#netmask_ipv4_3'))) {
            error++;
          }

          if (! validation.checkIPv4NetmaskByteAndPrevious($('#netmask_ipv4_4'), $('#netmask_ipv4_3'))) {
            error++;
          }

          $('#ip_addr_prefix').val(validation.mask2CIDR($('#netmask_ipv4_1'), $('#netmask_ipv4_2'), $('#netmask_ipv4_3'), $('#netmask_ipv4_4')));

          if (error > 0) {
            $('#ip_addr_prefix').focus();
            $('#ip_addr_prefix').blur();
            // $('#glyph2-ok').hide();
            // $('#glyph2-ko').show();
          }
          else {
            $('#glyph2-ko').hide();
            $('#glyph2-ok').show();
          }
          validateIPForm();
        }
      });

      // Gateway
      $('#gateway_ipv4_1, #gateway_ipv4_2, #gateway_ipv4_3, #gateway_ipv4_4').on({
        keyup: function() {
          if ($('#gateway_ipv4_1').val() !== '' && $('#gateway_ipv4_2').val() !== '' && $('#gateway_ipv4_3').val() !== '' && $('#gateway_ipv4_4').val() !== '') {

            if (!validation.checkIPv4Byte1($('#gateway_ipv4_1')) || !validation.checkIPv4Byte23($('#gateway_ipv4_2')) || !validation.checkIPv4Byte23($('#gateway_ipv4_3')) || !validation.checkIPv4Byte4($('#gateway_ipv4_4')) || $('#gateway_ipv4_2').val() === '00' || $('#gateway_ipv4_2').val() === '000' || $('#gateway_ipv4_3').val() === '00' || $('#gateway_ipv4_3').val() === '000') {
              $('#glyph3-ok').hide();
              $('#glyph3-ko').show();
            }
            else {
              $('#glyph3-ko').hide();
              $('#glyph3-ok').show();
            }
          }
          else {
            $('#glyph3-ok').hide();
            $('#glyph3-ko').hide();
          }
          validateIPForm();
        }
      });

      // DNS
      $('#dns_1, #dns_2, #dns_3, #dns_4').on({
        keyup: function() {
          if ($('#dns_1').val() !== '' && $('#dns_2').val() !== '' && $('#dns_3').val() !== '' && $('#dns_4').val() !== '') {

            if (!validation.checkIPv4Byte1($('#dns_1')) || !validation.checkIPv4Byte23($('#dns_2')) || !validation.checkIPv4Byte23($('#dns_3')) || !validation.checkIPv4Byte4($('#dns_4')) || $('#dns_2').val() === '00' || $('#dns_2').val() === '000' || $('#dns_3').val() === '00' || $('#dns_3').val() === '000') {
              $('#glyph4-ok').hide();
              $('#glyph4-ko').show();
            }
            else {
              $('#glyph4-ko').hide();
              $('#glyph4-ok').show();
            }
          }
          else {
            $('#glyph4-ok').hide();
            $('#glyph4-ko').hide();
          }
          validateIPForm();
        }
      });

      // Function for validate Ip/Netmask/Gateway
      function validateIPForm() {
        var error = 0;
        if ( $('#ip_addr_ipv4_1').val() && $('#ip_addr_ipv4_2').val() && $('#ip_addr_ipv4_3').val() && $('#ip_addr_ipv4_4').val() ) {
          if (! validation.checkIPv4Byte1($('#ip_addr_ipv4_1')) ) {
            error++;
          }
          if (! validation.checkIPv4Byte23($('#ip_addr_ipv4_2')) ) {
            error++;
          }
          if (! validation.checkIPv4Byte23($('#ip_addr_ipv4_3')) ) {
            error++;
          }
          if (! validation.checkIPv4Byte4($('#ip_addr_ipv4_4')) ) {
            error++;
          }
        }
        else {
          error++;
        }

        if ( $('#netmask_ipv4_1').val() && $('#netmask_ipv4_2').val() && $('#netmask_ipv4_3').val() && $('#netmask_ipv4_4').val() ) {
          if (! validation.checkIPv4NetmaskByte($('#netmask_ipv4_1')) ) {
            error++;
          }
          if (! validation.checkIPv4NetmaskByteAndPrevious($('#netmask_ipv4_2'), $('#netmask_ipv4_1')) ) {
            error++;
          }
          if (! validation.checkIPv4NetmaskByteAndPrevious($('#netmask_ipv4_3'), $('#netmask_ipv4_2')) ) {
            error++;
          }
          if (! validation.checkIPv4NetmaskByteAndPrevious($('#netmask_ipv4_4'), $('#netmask_ipv4_3')) ) {
            error++;
          }
          if (validation.mask2CIDR($('#netmask_ipv4_1'), $('#netmask_ipv4_2'), $('#netmask_ipv4_3'), $('#netmask_ipv4_4')) != $('#ip_addr_prefix').val()) {
            error++;
          }
        }
        else {
          error++;
        }

        if ( $('#gateway_ipv4_1').val() && $('#gateway_ipv4_2').val() && $('#gateway_ipv4_3').val() && $('#gateway_ipv4_4').val() ) {
          if (! validation.checkIPv4Byte1($('#gateway_ipv4_1')) ) {
            error++;
          }
          if (! validation.checkIPv4Byte23($('#gateway_ipv4_2')) ) {
            error++;
          }
          if (! validation.checkIPv4Byte23($('#gateway_ipv4_3')) ) {
            error++;
          }
          if (! validation.checkIPv4Byte4($('#gateway_ipv4_4')) ) {
            error++;
          }
        }
        else if ( (! $('#gateway_ipv4_1').val()) && (! $('#gateway_ipv4_2').val()) && (! $('#gateway_ipv4_3').val()) && (! $('#gateway_ipv4_4').val()) ) {
          // nothing
        }
        else {
          error++;
        }

        if ( $('#dns_1').val() && $('#dns_2').val() && $('#dns_3').val() && $('#dns_4').val() ) {
          if (! validation.checkIPv4Byte1($('#dns_1')) ) {
            error++;
          }
          if (! validation.checkIPv4Byte23($('#dns_2')) ) {
            error++;
          }
          if (! validation.checkIPv4Byte23($('#dns_3')) ) {
            error++;
          }
          if (! validation.checkIPv4Byte4($('#dns_4')) ) {
            error++;
          }
        }
        else {
          error++;
        }

        if (error) {
          $('#applyIP').attr('disabled', 'true');
          return false;
        }
        else{
          $('#applyIP').removeAttr('disabled');
          return true;
        }
      }

      // Function for reload window, with setTimeout
      function delayReloadWindow() {
        location.reload(true);
      }

      // Apply config
      $('#applyIP').on({
        click: function() {
          if ($("#LabelAutoIP").hasClass('active')) {
            $.ajax({
              url: "processing/ip-cgi.php",
              type: "post",
              data: $("#ipForm").serialize(),
              dataType: 'json',
              success: function(json) {
                if (json.status === "ok") {
                  $('#alert-config').text('Changes have been succefully saved');
                  $('#alert-config').addClass('alert-success');
                  $('#alert-config').delay(2000).addClass('in').show().fadeOut(300);
                  setTimeout(delayReloadWindow, 1500);
                }
                else if (json.status === "error") {
                  $('#alert-config').text(json.message);
                  $('#alert-config').addClass('alert-danger');
                  $('#alert-config').delay(5000).addClass('in').show().fadeOut(300);
                }
                else {
                  console.log(json.status + "\n" + json.message);
                }
              },
              fail: function() {
                alert("error on server");
                console.log("error on server");
              }
            });
          }
          else {
            if (!validateIPForm()) {
              $('#alert-config').text('Error on IP form');
              $('#alert-config').addClass('alert-warning');
              $('#alert-config').delay(800).addClass('in').show().fadeOut(300);
            }
            else {
              $.ajax({
                url: "processing/ip-cgi.php",
                type: "post",
                data: $("#ipForm").serialize(),
                dataType: 'json',
                success: function(json) {
                  if (json.status === "ok") {
                    $('#alert-config').text('Changes have been succefully saved');
                    $('#alert-config').addClass('alert-success');
                    $('#alert-config').delay(2000).addClass('in').show().fadeOut(300);
                    setTimeout(delayReloadWindow, 1500);
                  }
                  else if (json.status === "error") {
                    $('#alert-config').text(json.message);
                    $('#alert-config').addClass('alert-danger');
                    $('#alert-config').delay(5000).addClass('in').show().fadeOut(300);
                  }
                  else {
                    console.log(json.status + "\n" + json.message);
                  }
                },
                fail: function() {
                  alert("error on server");
                  console.log("error on server");
                }
              });
            }
          }
        }
      });
    </script>
  </body>
</html>
