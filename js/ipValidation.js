var ipValidation = function() {

	/* Private method */
	isNumeric = function(value) {
		var ValidChars = "0123456789";
		var IsNumber = true;
		var Char;

		for (i = 0; i < value.length && IsNumber == true; i++) { 
			Char = value.charAt(i);
			if (ValidChars.indexOf(Char) == -1) {
				IsNumber = false;
			}
		}
		return IsNumber;
	}
};

/* Public method */
ipValidation.prototype = {
	checkIPv4Byte1: function (field) {
		var val = field.val();
		if ( (val != "") &&
			 (((isNumeric.call(this, val)) == false) ||
			 (val < 1) ||
			 (val > 223) ) ) {
			return false;
		}
		else
			return true;
	},
	checkIPv4Byte23: function(field) {
		var val = field.val();
		if ( (val != "") &&
		     (((isNumeric.call(this, val)) == false) ||
		     (val < 0) ||
		     (val > 254) ) ) {
			return false;
		}
		else
			return true;
	},
	checkIPv4Byte4: function(field) {
		var val = field.val();
		if ( (val != "") &&
		      (((isNumeric.call(this, val)) == false) ||
		      (val < 1) ||
		      (val > 254) ) ) {
			return false;
		}
		else
			return true;
	},
	checkIPv4NetmaskByte: function(field) {
		var val = field.val();
		
		if (!val || val == "" || !isNumeric.call(this, val))
			return false;
		
		var maskByte = parseInt(val);
		
		switch(maskByte) {
		case 255:
		case 254:
		case 252:
		case 248:
		case 240:
		case 224:
		case 192:
		case 128:
		case 0:
			return true;
			break;
		}
		return false;
	},
	checkIPv4NetmaskByteAndPrevious: function(currentField, previousField) {
		var currentVal = currentField.val();
		var previousVal = previousField.val();
		if (this.checkIPv4NetmaskByte(currentField)) {
			if (previousField && parseInt(previousVal) != 255) {	/* If previous netmask field was not 255, we can only expect a 0 is subsequent netmask fields */
				if (currentVal != 0)
					return false;	/* Previous byte was not 255, this netmask byte must be 0 */
				else
					return true;
			}
			return true;
		}
		else
			return false;
	},
	mask2CIDR: function(maskField1, maskField2, maskField3, maskField4) {
		var mask = maskField1.val() + '.' + maskField2.val() + '.' +  maskField3.val() + '.' +  maskField4.val();
		var maskNodes = mask.match(/(\d+)/g);
		var cidr = 0;
		for(var i in maskNodes) {
			cidr += (((maskNodes[i] >>> 0).toString(2)).match(/1/g) || []).length;
		}
		return cidr;
	},
	CIDR2Mask: function(CIDRField) {
		var CIDRValue = CIDRField.val();
		var netmaskField1Value;
		var netmaskField2Value;
		var netmaskField3Value;
		var netmaskField4Value;

		/*if (CIDRValue < 0 || CIDRValue > 32) {
			return false;
		}*/
		if (CIDRValue <= 8) { /* Host/network boundary is within first byte */
		//~ alert("prefix boundary at 1st byte");
			netmaskField1Value = (0xff << (8-CIDRValue)) & 0xff;
		}
		else {
			netmaskField1Value = 0xff;
		}
		
		if (CIDRValue <= 16) { /* Host/network boundary is within second byte */
			if (CIDRValue <= 8)
				netmaskField2Value = 0;
			else
				netmaskField2Value = (0xff << (16-CIDRValue)) & 0xff;
		}
		else {
			netmaskField2Value = 0xff;
		}
		
		if (CIDRValue <= 24) { /* Host/network boundary is within third byte */
		//~ alert("prefix boundary at 3rd byte");
			if (CIDRValue <= 16)
				netmaskField3Value = 0;
			else
				netmaskField3Value = (0xff << (24-CIDRValue)) & 0xff;
		}
		else {
			netmaskField3Value = 0xff;
		}
		
		if (CIDRValue <= 32) { /* Host/network boundary is within fourth byte */
		//~ alert("prefix boundary at 4th byte");
			if (CIDRValue <= 16)
				netmaskField4Value = 0;
			else
				netmaskField4Value = (0xff << (32-CIDRValue)) & 0xff;
		}
		else {
			netmaskField4Value = 0xff;
		}
		return {
			netmaskField1Value: netmaskField1Value,
			netmaskField2Value: netmaskField2Value,
			netmaskField3Value: netmaskField3Value,
			netmaskField4Value: netmaskField4Value
		}
	}
};
