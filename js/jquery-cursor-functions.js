(function( $ ){
  jQuery.fn.getCursorPosition = function() {
    if(this.lengh == 0) return -1;
    return $(this).getSelectionStart();
  }
 
  jQuery.fn.getSelectionStart = function() {
    if(this.lengh == 0) return -1;
    input = this[0];
 
    var pos = input.value.length;
 
    if (input.createTextRange) {
      var r = document.selection.createRange().duplicate();
      r.moveEnd('character', input.value.length);
      if (r.text == '') 
        pos = input.value.length;
      pos = input.value.lastIndexOf(r.text);
    }
    else if(typeof(input.selectionStart)!="undefined")
      pos = input.selectionStart;
 
    return pos;
  }
 
  jQuery.fn.getSelectionEnd = function() {
    if(this.lengh == 0) return -1;
    input = this[0];
 
    var pos = input.value.length;
 
    if (input.createTextRange) {
      var r = document.selection.createRange().duplicate();
      r.moveStart('character', -input.value.length);
      if (r.text == '') 
        pos = input.value.length;
      pos = input.value.lastIndexOf(r.text);
    }
    else if(typeof(input.selectionEnd)!="undefined")
      pos = input.selectionEnd;
 
    return pos;
  }

  jQuery.fn.getSelectionSize = function() {
    return (this.getSelectionEnd() - this.getSelectionStart());
  }
})( jQuery );
