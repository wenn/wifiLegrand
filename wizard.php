		<?php
			include('include/inc_header.php');

			if(isset($_SESSION['ip']) && isset($_SESSION['user-agent']) && isset($_SESSION['identifier'])) {
				if (($_SESSION['ip'] != $_SERVER['REMOTE_ADDR']) || ($_SESSION['user-agent'] != $_SERVER['HTTP_USER_AGENT'])) {
					header("Location: index.php");
					exit;
				}
			}
			else {
				header("Location: index.php");
				exit;
			}
		?>
		<!-- Wizard page CSS -->
		<link href="css/wizard.css" rel="stylesheet">
		<style>
			#resultSSID, #resultWiFiKey, #resultHostname, #resultPassword {
				white-space: pre;
			}
		</style>
	</head>

	<body>
		<!-- Section pour l'animation du spinner -->
		<div id="spinner-load" style="display:none;">
			<div class="imgwrapper"> <!-- Container du spinner -->
				<img src="images/spinner.png" class="img-responsive turn" alt="loading spinner">
			</div>
			<div id="count">100%</div> <!-- Position du compteur -->
		</div>
		<!-- Fin section pour l'animation du spinner -->

		<!-- Début forbidLandscape -->
		<div class="forbidLandscape hidden-sm hidden-lg hidden-md hidden">
			<div class="forbidLandscapeTable">
				<div class="forbidLandscapeTableCell">
					<p class="forbidLandscapeTableCellParaph text-center">
						<?= _('Veuillez utiliser votre smartphone en mode portrait'); ?>
					</p>
				</div>
			</div>
		</div><!-- Fin forbidLandscape -->

		<form role="form" data-toggle="validator" id="formWizard" method="post" action=""> <!-- Début formulaire -->
			<input type="hidden" id="inputCode" name="inputCode">
			<!-- Début Carousel -->
			<div id="carousel-wizard" class="carousel slide" data-ride="carousel" data-interval="false" style="position: fixed">
				<!-- Wrapper for slides --> <!--data-interval="false"-->
				<div class="carousel-inner">
					<!-- STEP 1-->
					<div id="carousel-step1" class="item active">
						<div class="container-fluid">
							<!-- Header du carousel pour le réseau -->
							<div class="row header">
								<!--<div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
								<img class="img-responsive" src="images/wizard_step2.png">
								</div>-->
								<div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 hidden-xs"> <!-- Titre de l'étape version desktop -->
									<h1 class="font-title"><!--{$wizard_step2_title}--><?= _('Réseau'); ?></h1>
								</div> <!-- Fin titre de l'étape -->

								<div class="hidden-lg hidden-md hidden-sm visible-xs col-xs-12 margintop"> <!-- Titre de l'étape version mobile -->
									<h1 class="font-title majuscule"><?= _('Réseau'); ?></h1>
								</div> <!-- Fin titre de l'étape -->

								<!-- Début choix de la langue -->
								<div class="col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 hidden-sm hidden-xs">
									<!-- Select language -->
									<select class="flag-option font-title-select" name="languageOption" id="langs">
										<option value="fr" id="fr">Français</option>
										<option value="en" id="en">English</option>
										<option value="it" id="it">Italiano</option>
										<option value="es" id="es">Español</option>
										<option value="pt" id="pt">Português</option>
										<option value="ru" id="ru">Русский</option>
									</select>
									<div class="alert alert-danger hidden" id="langError" role="alert"></div>
								</div> <!-- Fin choix de la langue -->
							</div> <!-- Fin Header du container -->

							<!--  Content du carousel -->
							<div class="row content content-step-2 margintop">
								<!-- Curseur prev -->
									<!--<div class="col-lg-1 col-md-1 hidden-sm hidden-xs">
										<a href="javascript:;" id="inputPage1Prev-lg-md" class="pull-left"><img class="customCursor" src="images/prev.png" alt="previous cursor"></a>
									</div>-->
								<div class="col-lg-offset-1 col-lg-11 col-md-offset-1 col-md-11 col-sm-offset-1 col-sm-11 col-xs-12 col-xs-offset-0">
									<div class="row">
										<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
											<div class="form-group has-feedback group-feedback">
												<label for="inputWiFiZone" class="font-label">
													<!--{$wizard_zone}--><?= _('Zone WiFi'); ?>
													<a class="help_link" tabindex="0" data-toggle="tab" data-placement="right" title="Choisir le pays où le point d'accès est installé permet de s'adapter aux réglementations du pays en terme de bande de fréquences et de puissance d'émission.">&nbsp;
														<img src="./images/help_icon.png" class="helpIcon" alt="icon help">
													</a>
												</label>

												<select name="inputWiFiZone" class="form-control input-select custom-select" id="inputWiFiZone" tabindex="1">
													<!--{html_options values=$wifizone_ids output=$wifizone_names selected=$wifizone}-->
													<option value="fr" class="countryZone">France</option>
													<option value="en" class="countryZone">United Kingdom</option>
													<option value="it" class="countryZone">Italia</option>
													<option value="es" class="countryZone">Spain</option>
													<option value="po" class="countryZone">Portugal</option>
													<option value="ru" class="countryZone">Russia</option>
													<option value="ius" class="countryZone">United States</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 form-element">
											<div class="form-group has-feedback group-feedback">
												<label for="inputSSID" class="font-label">
													<?= _('Nom du réseau (SSID)'); ?>
												</label>
												<br>
												<input type="text" class="form-control custom-input input-text custom-input-field" id="inputSSID" name="inputSSID" tabindex="2" data-maxlength="31" data-bind="value: ssid" placeholder="<?= _('Sélectionner un réseau'); ?>" autofocus required>
												<span class="glyphicon form-control-feedback field-feedback pull-right" id="inputSSIDSpan" aria-hidden="true"></span>
												<div class="help-block with-errors hidden"></div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 form-element">
											<div class="form-group has-feedback group-feedback">
												<label for="inputWiFiKey" class="font-label"><!--{$wizard_cle}--><?= _('Clé Wifi'); ?></label>
												<br/>
												<input type="text" class="form-control custom-input input-text custom-input-field" id="inputWiFiKey" name="inputWiFiKey" tabindex="3" data-maxlength="30" data-minlength="8" placeholder="<?= _('Saisir la clé wifi'); ?>" autofocus required><!--placeholder="{$wizard_cle_placeholder}"   {if isset($wifikey)}value="{$wifikey}"{/if}-->
												<span class="glyphicon form-control-feedback field-feedback pull-right" id="inputWiFiKeySpan"><!--<img class="img-feedback" src="images/tick-icon.png">--></span>
												<div class="help-block with-errors hidden"></div>
											</div>
										</div>
									</div>

			  					<!-- boutons version desktop -->
			  					<div class="row hidden-xs">
			  						<div class="col-lg-5 col-md-5 col-sm-5">
			  							<div class="f1-buttons pull-left">
			  								<input type="button" id="validate-step1-desktop" class="prev-next btn btn-submit" value="<?= _('Suivant'); ?>" disabled="true">
			  							</div>
			  						</div>
			  						<div class="clearfix"></div>
			  					</div>
			  					<!-- Fin boutons version desktop -->



								</div> <!-- Fin col-lg-offset-1 col-lg-offset-11, ... -->
							</div> <!-- Fin row-content -->
							<div class="row footer hidden-xs visible-lg visible-md visible-sm"> <!-- Début row footer pour step 1 et pour version desktop -->
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs margintop">
										<div class=" col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-offset-1 col-sm-2">
											<img class="img-responsive paddingtop" src="images/legrand_logo.png" alt="Logo Legrand">
										</div>
		  							<div class="col-lg-offset-0 col-lg-9 col-md-offset-0 col-md-9 col-sm-offset-0 col-sm-9"><!--version desktop -->
		  								<div class="f1-steps">
		  									<div class="f1-step active">
		  										<div class="f1-step-icon">
		  											<img src="images/step01_device_ok_icon.png" alt="icon identification" class="block-center">
		  										</div>
		  										<p class="button-inactive"><?= _('Identification'); ?></p>
		  									</div>
		  									<span id="bar1" class="progress_bar"></span>
		  									<div class="f1-step active">
		  										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  										<div class="f1-step-icon">
		  											<img src="images/step02_wifi_ok_icon.png" alt="icon Network"   class="block-center">
		  										</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  										<p class="blanc button-active"><?= _('Réseau'); ?></p>
		  									</div>
		  									<span id="bar2" class="progress_bar"></span>
		  									<div class="f1-step">
		  										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  										<div class="f1-step-icon">
		  											<img src="images/step03_acess_point_ko_icon.png" alt="icon access point"  class="block-center">
		  										</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  										<p class="button-inactive"><?= _('Point d\'accès'); ?></p>
		  									</div>
		  								</div>
		  							</div>
		  							<div class="clearfix"></div>
		  						</div>
		  					</div>
		  				</div>
	  					<!-- Barre de progression version mobile -->
	  					<div class="row footer hidden-lg hidden-md hidden-sm visible-xs">
              <!-- Boutons version mobile -->
                  <div class="row hidden-lg hidden-md hidden-sm visible-xs">
                    <div class="col-xs-offset-1 col-xs-10">
                      <input type="button" id="validate-step1-mobile" class="prev-next btn btn-block btn-submit" value="<?= _('Suivant'); ?>" disabled="true">
                      <div class="clearfix"></div>
                    </div>
                  </div> <!-- Fin row hidden-lg hidden-md .. -->
	  						<div class="progress" style="position:absolute; bottom:0;left:0;width:100%;">
	  							<div class="progress-bar progress-bar-pink" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:66%;">
	  								<span class="sr-only"><?= _('66% effectué'); ?></span>
	  							</div>
	  						</div><!-- Fin barre de progression -->
	  					</div> <!-- Fin footer -->
	  				</div> <!-- /container fluid -->
	  			</div> <!-- Fin item active -->

		  		<!-- STEP 2-->
		  		<div id="carousel-step2" class="item">
		  			<div class="container-fluid">
		  				<!-- Header du carousel pour le point d'accès -->
		  				<div class="row header acces-step-3">
							<!--<div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
							<img class="img-responsive" src="images/wizard_step3.png">
							</div>-->
								<div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 hidden-xs"> <!-- Titre de l'étape version desktop -->
									<h1 class="font-title"><!--{$wizard_step3_title}--><?= _('Point d\'accès'); ?></h1>
								</div> <!-- Fin titre de l'étape -->

								<div class="hidden-lg hidden-md hidden-sm col-xs-12 margintop"> <!-- Titre de l'étape version mobile -->
									<h1 class="font-title majuscule"><?= _('Point d\'accès'); ?></h1>
								</div> <!-- Fin titre de l'étape -->

								<!-- Début choix de la langue -->
								<div class="col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 hidden-sm hidden-xs">
									<!-- Select language -->
									<select class="flag-option font-title-select" name="languageOption2" id="langs2">
										<option value="fr" id="fr">Français</option>
										<option value="en" id="en">English</option>
										<option value="it" id="it">Italiano</option>
										<option value="es" id="es">Español</option>
										<option value="pt" id="pt">Português</option>
										<option value="ru" id="ru">Русский</option>
									</select>
									<div class="alert alert-danger hidden" id="langError" role="alert"></div>
								</div> <!-- Fin choix de la langue -->
							</div> <!-- Fin Header du container -->
							<!--  Content du carousel -->
							<div class="row content content-step-3">
									<!--<div class="col-lg-1 col-md-1 hidden-sm hidden-xs">
										<a href="javascript:;" id="inputPage2Prev-lg-md" class="pull-left"><img src="images/prev.png"></a>
									</div>-->
								<div class="col-lg-offset-1 col-lg-11 col-md-offset-1 col-md-11 col-sm-offset-1 col-sm-11 col-xs-12 col-xs-offset-0">
									<div class="row">
										<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
											<div class="form-group has-feedback group-feedback">
												<label for="inputHostname" class="font-label">
												<!--{$wizard_nom}--> <?= _('Nom du point d\'accès'); ?>
													<a class="help_link" tabindex="0" data-toggle="tab" data-placement="right" title="Indiquer un nom permet de retrouver votre équipement dans votre installation grace à des outils de découverte.">
														&nbsp;<img src="./images/help_icon.png" class="helpIcon" alt="icon help">
													</a>
												</label>
												<br>
												<input type="text" class="form-control custom-input input-text custom-input-field" id="inputHostname" name="inputHostname" tabindex="5" required autofocus  placeholder="<?= _('Point d\'accès 1'); ?>">
												<!-- placeholder="{$wizard_nom_placeholder}" {if isset($hostname)}value="{$hostname}"{/if}-->
												<span class="glyphicon form-control-feedback field-feedback pull-right" id="inputHostnameSpan" aria-hidden="true"><!--<img class="img-feedback" src="images/tick-icon.png" alt="tick-icon"--></span>
												<div class="help-block with-errors hidden"></div>
											</div>
										</div>
									</div> <!-- fin row -->
									<div class="row">
										<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 form-element">
											<div class="form-group has-feedback group-feedback">
												<label for="inputPassword" class="font-label">
													<!--{$wizard_mdp}--> <?= _('Mot de passe'); ?>
													<a class="help_link" tabindex="0" data-toggle="tab" data-placement="right" title="Ce mot de passe vous permettra de vous connecter à l'interface d'administration de votre point d'accès Wi-Fi afin de modifier ses paramètres.">
														&nbsp;<img src="./images/help_icon.png" class="helpIcon" alt="icon help">
													</a>
												</label>
												<br/>
												<input type="password" class="form-control custom-input input-text custom-input-field" id="inputPassword" name="inputPassword" tabindex="6" minLength="8" required autofocus placeholder="<?= _('Votre mot de passe'); ?>">
												<!--  placeholder="{$wizard_mdp_placeholder}" {if isset($password)}value="{$password}"{/if} -->
												<span class="glyphicon form-control-feedback field-feedback pull-right" id="inputPassword" aria-hidden="true"><!--<img class="img-feedback" src="images/tick-icon.png" alt="tick-icon"--></span>
												<div class="help-block with-errors"></div>
											</div>
										</div>
									</div> <!-- Fin row -->

									<!-- Button step 2 desktop -->
									<div class="row hidden-xs">
										<div class="col-lg-5 col-md-5 col-sm-5">
											<div class="f1-buttons pull-left">
												<input type="button" id="previous-step2-desktop" class="prev-next btn btn-previous" value="<?= _('Précédent'); ?>">
												<input type="button" id="display-modal-desktop" data-toggle="modal" data-target="#squarespaceModalDesktop" class="prev-next btn btn-next" value="<?= _('Finaliser'); ?>" disabled="true">
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<!-- Fin boutons step2 version desktop -->

									<!-- Button step 2 mobile -->
									<div class="row hidden-lg hidden-md hidden-sm">
										<div class="col-xs-12">
											<input type="button" class="prev-next btn btn-previous pull-left" value="<?= _('Précédent'); ?>">
											<input type="button" data-toggle="modal" data-target="#squarespaceModalMobile" class="prev-next btn btn-submit pull-right" id="display-modal-mobile" value="<?= _('Finaliser'); ?>" disabled="true">
											<div class="clearfix"></div>
										</div>
									</div>

									<!-- Modal version desktop -->
									<div class="modal fade hidden-xs" id="squarespaceModalDesktop" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="true">
										<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 margin-top">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header content-resume">
														<h2 class="modal-title font-title" id="lineModalLabel"><?= _('Rappel'); ?></h2>
													</div>
													<div class="modal-body">
														<!-- content goes here -->
														<div class="row content-resume-item">
															<div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 font-modal"><!--{$wizard_zone}--> <?= _('Zone wifi'); ?></div>
															<div class="col-lg-5 col-lg-offset-2 col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-offset-0 col-xs-6 font-input-modal" id="resultWiFiZone"></div>
														</div>
														<div class="row content-resume-item">
															<div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 font-modal"><!--{$wizard_SSID}--> <?= _('Réseau'); ?></div>
															<div class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-offset-0 col-xs-6 font-input-modal" id="resultSSID"></div>
														</div>
														<div class="row content-resume-item">
															<div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 font-modal"><!--{$wizard_cle}--> <?= _('Clé Wifi'); ?></div>
															<div class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-offset-0 col-xs-6 font-input-modal" id="resultWiFiKey">mysecretkey</div>
														</div>
														<div class="row content-resume-item">
															<div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 font-modal"><!--{$wizard_nom}--><?= _('Point d\'accès'); ?></div>
															<div class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-offset-0 col-xs-6 font-input-modal"  id="resultHostname"></div>
														</div>
														<div class="row content-resume-item">
															<div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 font-modal"><!--{$wizard_mdp}--><?= _('Mot de passe'); ?></div>
															<div class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-offset-0 col-xs-6 font-input-modal"  id="resultPassword">myadminpassword</div>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="modal-footer">
														<small id="configError" style="color:rgb(255,119,127)"></small>
														<div>
															<a type="button" id="cancelConfig-modal" class="btn btn-group  btn-white2 btn-delete hell" data-dismiss="modal" role="button" style="width:70%"><?= _('Annuler'); ?></a>

															<a type="button" id="applyConfig-modal" class="btn btn-group btn-pink2" data-action="save" role="button" style="width:29%"><?= _('Configurer'); ?></a>
														</div>
														<div class="clearfix"></div>
													</div> <!-- Fin modal footer -->
												</div> <!-- Fin modal content -->
											</div> <!-- Fin modal-dialog -->
										</div> <!-- Fin grid pour la modal -->
									</div> <!-- Fin modal version desktop -->

									<!-- Modal version mobile -->
									<div class="modal fade hidden-lg hidden-md hidden-sm" id="squarespaceModalMobile" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="true">
										<div class="col-xs-12">
											<div class="modal-dialog modal-padding">
												<div class="modal-content mobileversion">
													<div class="modal-header mobileversion content-resume">
														<span class="pull-left">
															<a type="button" class="btn-delete" data-dismiss="modal" id="delImage" role="button">
																<img src="images/right-thin-chevron.png" class="img-responsive size-chevron" alt="cursor previous">
															</a>
														</span>
														<h2 class="modal-title font-title" id="lineModalLabel"><?= _('Rappel'); ?></h2>
														<div class="clearfix"></div>
													</div>
													<div class="modal-body">
														<!-- content goes here -->
														<div class="row content-resume-item-mobile">
															<div class="col-xs-6 font-modal-mobile"><!--{$wizard_zone}--> <?= _('Zone wifi'); ?></div>
															<div class="col-xs-6 font-input-modal-mobile" id="resultWiFiZoneMobile"></div>
														</div>
														<div class="row content-resume-item-mobile">
															<div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 font-modal-mobile"><!--{$wizard_SSID}--> <?= _('Réseau'); ?></div>
															<div class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-offset-0 col-xs-6 font-input-modal-mobile" id="resultSSIDMobile"></div>
														</div>
														<div class="row content-resume-item-mobile">
															<div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 font-modal-mobile"><!--{$wizard_cle}--> <?= _('Clé Wifi'); ?></div>
															<div class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-offset-0 col-xs-6 font-input-modal-mobile" id="resultWiFiKeyMobile">ok</div>
														</div>
														<div class="row content-resume-item-mobile">
															<div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 font-modal-mobile"><!--{$wizard_nom}--><?= _('Point d\'accès'); ?></div>
															<div class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-offset-0 col-xs-6 font-input-modal-mobile"  id="resultHostnameMobile"></div>
														</div>
														<div class="row content-resume-item-mobile">
															<div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 font-modal-mobile"><!--{$wizard_mdp}--><?= _('Mot de passe'); ?></div>
															<div class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-offset-0 col-xs-6 font-input-modal-mobile"  id="resultPasswordMobile"></div>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="modal-footer">
														<div>
															<small id="configErrorMobile" style="color:rgb(255,119,127)"></small>
															<input type="button" id="applyConfig-modal-mobile" class="btn-block prev-next btn btn-submit" data-action="save" role="button" value=<?= _('Configurer'); ?>>
														</div>
														<div class="clearfix"></div>
													</div>
												</div> <!-- Fin modal content -->
											</div> <!-- Fin modal-dialog -->
										</div> <!-- Fin grid pour la modal -->
									</div> <!-- Fin modal version mobile -->
								</div> <!-- Fin grid col-lg-11 ... etc -->
							</div> <!-- Fin row-content -->

							<div class="row footer hidden-xs visible-lg visible-md visible-sm"> <!-- Début row footer pour step 1 et pour version desktop -->
		  					<div class="margintop"></div>
		  					<div class="row">
		  						<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
		  							<div class=" col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-offset-1 col-sm-2">
		  								<img class="img-responsive paddingtop" src="images/legrand_logo.png" alt="Logo Legrand">
		  							</div>
		  							<div class="col-lg-offset-0 col-lg-9 col-md-offset-0 col-md-9 col-sm-offset-0 col-sm-9"><!--version desktop -->
		  								<div class="f1-steps">
		  									<div class="f1-step active">
		  										<div class="f1-step-icon"><img src="images/step01_device_ok_icon.png" alt="icon identification" class="block-center"></div>
		  										<p class="button-inactive"><?= _('Identification'); ?></p>
		  									</div>
	  										<span id="bar1" class="progress_bar"></span>
	  										<div class="f1-step active">
	  											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="f1-step-icon"><img src="images/step02_wifi_ok_icon.png" alt="icon Network"   class="block-center"></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  											<p class="blanc button-inactive"><?= _('Réseau'); ?></p>
	  										</div>
	  										<span id="bar1" class="progress_bar"></span>
	  										<div class="f1-step active">
	  											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="f1-step-icon"><img src="images/step03_acess_point_ok_icon.png" alt="icon access point"  class="block-center"></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  											<p class="blanc button-active"><?= _('Point d\'accès'); ?></p>
	  										</div>
	  									</div>
		  							</div>
		  							<div class="clearfix"></div>
		  						</div>
		  					</div>
		  				</div>
							<!-- Barre de progression version mobile -->
							<div class="row footer step2-acces hidden-lg hidden-md hidden-sm visible-xs">
								<div class="progress" style="position:absolute; bottom:0;left:0;width:100%;">
									<div class="progress-bar progress-bar-pink" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:85%;">
										<span class="sr-only"><?= _('85% effectué'); ?></span>
									</div>
								</div>
							</div><!-- Fin barre de progression -->
						</div> <!-- /container fluid -->
					</div> <!-- Fin item step 2 -->

					<!-- STEP 3-->
					<div id="carousel-step3" class="item">
						<div class="container-fluid">
							<!-- Header -->
							<div class="row header step4-header">
								<div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 hidden-xs"> <!-- Titre de l'étape version desktop -->
									<h1 class="font-title"><!--{$wizard_appliquer_title}-->Bravo !</h1>
								</div> <!-- Fin titre de l'étape -->
								<div class="hidden-lg hidden-md hidden-sm col-xs-12 margintop"> <!-- Titre de l'étape version mobile -->
									<h1 class="font-title majuscule text-center"><!--{$wizard_appliquer_title}-->Configuration réussie !</h1>
								</div> <!-- Fin titre de l'étape -->
								<!-- Début choix de la langue -->
								<div class="col-lg-offset-2 col-lg-2 col-md-offset-2 col-md-2 col-sm-offset-2 col-sm-2 hidden-xs">
									<!-- Select language -->
									<select class="flag-option" name="languageOption3" id="langs3" disabled>
										<option value="fr" id="fr">Français</option>
										<option value="en" id="en">English</option>
										<option value="it" id="it">Italiano</option>
										<option value="es" id="es">Español</option>
										<option value="pt" id="pt">Português</option>
										<option value="ru" id="ru">Русский</option>
									</select>
									<div class="alert alert-danger hidden" id="langError" role="alert"></div>
								</div> <!-- Fin choix de la langue -->
							</div>

							<!-- Content -->
							<div class="row content content-step-4">
								<div class="col-lg-offset-1 col-lg-11 col-md-offset-1 col-md-11 col-sm-offset-1 col-sm-11 col-xs-12 margin-xs">
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
											<p class="font-info">La configuration a été appliquée avec succès. <br>
											Le point d'accès sera disponible après le redémarrage automatique.<!--{$wizard_appliquer_text_1}--></p>
										</div>
									</div>
									<div class="row margintop">
										<div class="hidden-lg hidden-md hidden-sm col-xs-12">
											<p class="font-info-xs text-center">
											Le point d'accès sera disponible après le redémarrage automatique.<!--{$wizard_appliquer_text_1}--></p>
										</div>
									</div>
									<div class="row hidden-xs">
										<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
											<div class="media">
												<div class="media-left pull-left">
													<img src="images/ampoule2.png" alt="icone ampoule">
										  	</div>
												<div class="media-body blanc">
													<blockquote><!--{$wizard_appliquer_text_2}-->Nous vous conseillons de sauvegarder vos réglages. Ces informations pourront vous être utiles ultérieurement</blockquote>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="hidden-lg hidden-md hidden-sm col-xs-12">
											<div class="media">
												<div class="media-body text-center">
													<div class="media-left padtop-xs pull-left">
									 					<a href="#">
															<img src="images/ampoule-xs.png" class="center-block" alt="icone ampoule">
														</a>
													</div>
													<p class="bleu"><!--{$wizard_appliquer_text_2}--><small>Nous vous conseillons de sauvegarder vos réglages. Ces informations peuvent vous être utiles ultérieurement</small></p>
												</div>
											</div>
										</div>
									</div>


									<div class="row hidden-xs hidden-sm">
										<div class="col-lg-3 col-md-3 col-sm-4">
										  <div class="center-block space-desktop">
												<input type="button" class="btn btn-pdf pdf-sms-email btn-block" value="Enregistrer en pdf (recommandé)" id="savePDF-lg-md">
												<div class="space-desktop"></div>
												<input type="button" class="btn btn-sms-email pdf-sms-email btn-block" id="sendEmail-lg-md" value="Envoyer par email*">
												<div class="space-desktop"></div>
													<!--<img src="images/pdf-icon.png">&nbsp;&nbsp;{$wizard_PDF}-->
												<p class="bleu"><small>* Selon tarif en vigueur chez votre opérateur</small></p>
											</div>
										</div>
									</div>

									<div class="row hidden-xs hidden-lg hidden-md visible-sm">
										<div class="col-sm-4">
										 	<div class="center-block space-desktop">
												<input type="button" class="btn btn-pdf pdf-sms-email btn-block" value="Enregistrer en pdf (recommandé)" id="savePDF-sm">
												<div class="space-desktop"></div>
												<input type="button" class="btn btn-sms-email pdf-sms-email btn-block" id="sendSMS-sm" value="Envoyer par SMS*">
												<div class="space-desktop"></div>
												<input type="button" class="btn btn-sms-email pdf-sms-email btn-block" id="sendEmail-sm" value="Envoyer par email*">
												<div class="space-desktop"></div>
													<!--<img src="images/pdf-icon.png">&nbsp;&nbsp;{$wizard_PDF}-->
												<p class="bleu"><small>* Selon tarif en vigueur chez votre opérateur</small></p>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- Footer -->
							<div class="row footer step4 padtop">
								<div class="col-xs-12 hidden-lg hidden-md hidden-sm visible-xs">
									<div class="row">
										<div class="col-xs-offset-1 col-xs-10 space">
											<div class="center-block">
												<input type="button" class="btn btn-sms-email pdf-sms-email btn-block" id="sendSMS-xs" value="Envoyer par SMS*">
												<div class="space"></div>
													<!--<img src="images/pdf-icon.png">&nbsp;&nbsp;{$wizard_PDF}-->
												<input type="button" class="btn btn-sms-email pdf-sms-email btn-block" id="sendEmail-xs" value="Envoyer par email*">
												<div class="space"></div>
													<!--<img src="images/pdf-icon.png">&nbsp;&nbsp;{$wizard_PDF}-->
												<input type="button" class="btn btn-pdf pdf-sms-email btn-block" value="Enregistrer en pdf (recommandé)" id="savePDF-xs">
													<!--<img src="images/pdf-icon.png">&nbsp;&nbsp;{$wizard_PDF}-->
												<p><small>* Selon tarif en vigueur chez votre opérateur</small></p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
									<div class="row">
		      					<div class=" col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-offset-1 col-sm-2">
		        					<img class="img-responsive paddingtop" src="images/legrand_logo.png" alt="Logo Legrand">
		      					</div>
	    						</div>
								</div>
							</div> <!-- Fin row footer -->
	  				</div>
					</div> <!-- /container-fluid -->
				</div> <!-- Fin carousel step3 -->
			</div>
		</form>
		<!-- Placed at the end of the document so the pages load faster -->
		<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="js/jquery.touchSwipe.min.js"></script>
		<script src="js/jquery-cursor-functions.js"></script>
		<!-- Contrôle des input avec le js validator de bootstrap (Front) -->
		<script src="js/validator.js"></script>

		<script>
			// Pour empêcher la modale d'être derrière le background (problème posé à cause de la position du container qui est fixe)
			$('#squarespaceModalDesktop').appendTo("body");
			$('#squarespaceModalMobile').appendTo("body");
		</script>

		<script>
			// Faire avancer la barre de progression (version mobile) à 100% quand l'utilisateur clique sur le bouton finaliser
		 	$("#display-modal-mobile").click(function() {
		 		$('.progress-bar-pink').css('width', 100 + '%');
		 	});
		</script>

		<script>
		  // ToolTip Affichage des bulles infos pour certains champs
		  $(function(){
		  	$('a[title]').tooltip();
		  });
		</script>

		<script type="text/javascript">
			// var initial_screen_size;
			// var is_keyboard = false;
			// var viewport;
		  //
			// function findBootstrapEnvironment() {
			// 	var envs = ["ExtraSmall", "Small", "Medium", "Large"];
			// 	var envValues = ["xs", "sm", "md", "lg"];
		  //
			// 	var $el = $('<div>');
			// 	$el.appendTo($('body'));
		  //
			// 	for (var i = envValues.length - 1; i >= 0; i--) {
			// 		var envVal = envValues[i];
		  //
			// 		$el.addClass('hidden-'+envVal);
			// 		if ($el.is(':hidden')) {
			// 			$el.remove();
			// 			return envs[i]
			// 		}
			// 	};
			// }
		  //
		  //
			// function updateViews() {
			// 	is_keyboard = (window.innerHeight < initial_screen_size);
			// 	if (is_keyboard) {
			// 		$('.footer-1, .footer-2').addClass("hidden");
			// 	}
			// 	else {
			// 		$('.footer-1, .footer-2').removeClass("hidden");
			// 	}
			// }
		  //
			// function doOnOrientationChange() {
			// 	switch(window.orientation) {
			// 		case -90:
			// 		case 90:
			// 		$(".forbidLandscape").removeClass("hidden");
			// 		break;
			// 		default:
			// 		$(".forbidLandscape").addClass("hidden");
			// 		break;
			// 	}
			// }
		  //
			// viewport = findBootstrapEnvironment();
		  //
			// $(document).ready(function() {
			// 	$('.carousel').carousel({
			// 		interval: false
			// 	});
		  //
			// 	if ( (viewport == 'ExtraSmall') || (viewport == 'Small')) {
		  //
			// 				//Enable swiping...
			// 				$(".carousel-inner").swipe({
			// 					//Generic swipe handler for all directions
			// 					swipeLeft: function(event, direction, distance, duration, fingerCount) {
			// 						var carouselData = $(this).parent().data('bs.carousel');
			// 						var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
		  //
			// 						if ( (currentIndex == 0) && validateStep1() ) {
			// 							$(this).parent().carousel('next');
			// 							$('#inputHostname').focus();
			// 						}
		  //
			// 						if ( (currentIndex == 1) && validateStep2() ) {
			// 							$(this).parent().carousel('next');
			// 						}
			// 					},
			// 					swipeRight: function() {
			// 						var carouselData = $(this).parent().data('bs.carousel');
			// 						var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
		  //
			// 						if ( (currentIndex == 1) || (currentIndex == 2) ) {
			// 							$(this).parent().carousel('prev');
			// 							if (currentIndex == 1) {
			// 								$('#inputWiFiZone').focus();
			// 							}
			// 							if (currentIndex == 2) {
			// 								$('#inputHostname').focus();
			// 							}
			// 						}
			// 					},
			// 					//Default is 75px, set to 0 for demo so any distance triggers swipe
			// 					threshold:0
			// 				});
			// 			}
		  //
		//~ var popoverTemplate = ['<div class="popover" role="tooltip">',
							//~ '<div class="arrow"></div>',
							//~ '<div class="help_header">',
							//~ '<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
							//~ '<h3 class="popover-title help_title"></h3>',
							//~ '</div>',
							//~ '<div class="popover-content help_fongt">',
							//~ '</div>',
							//~ '</div>'].join('');

						//~ $('[data-toggle="popover"]').popover({
							//~ template: popoverTemplate
						//~ }).parent().delegate('button.close', 'click', function() {
							//~ $('[data-toggle="popover"]').popover('hide')
						//~ });
		  //
			// 			$('#inputWiFiZone').focus();
		  //
			// 			validateStep1();
			// 			validateStep2();
		  //
			// 			initial_screen_size = window.innerHeight;
			// 			doOnOrientationChange();
			// 		});
		  //
			// 		/**
			// 		 * Gestion de l'orientation
			// 		 **/
			// 		window.addEventListener('orientationchange', doOnOrientationChange);
		  //
			// 		/**
			// 		 * Gestion clavier virtuel
			// 		 **/
		  //
			// 		 /* Android */
			// 		window.addEventListener('resize', updateViews);
		  //
			// 		 /* iOS */
			// 		$('#inputSSID, #inputWifiKey, #inputHostname, #inputPassword').bind("focus blur", function() {
			// 		 	$(window).scrollTop(10);
			// 		 	is_keyboard = $(window).scrollTop() > 0;
			// 		 	$(window).scrollTop(0);
			// 		 	updateViews();
			// 		 });
		  //



			// Recovery language in (php)$_SESSION and attribute to #langs, #langs2, #langs3
			var lang = "<?php echo $_SESSION['lang']; ?>";
			$("#langs option[value='" + lang + "']").attr('selected', 'selected');
			$("#langs2 option[value='" + lang + "']").attr('selected', 'selected');
			$("#langs3 option[value='" + lang + "']").attr('selected', 'selected');

			/**
			 * Change Language
			 **/
			$('#langs, #langs2').on('change', function() {
				if(this.value != '') {
					$.ajax({
						url: "processing/i18n-identification-cgi.php",
						type: "post",
						data: "lang=" + $("#langs option:selected").val(),
						dataType: 'json',
						success: function(json) {
							if (json.reponse === "ok") {
								location.reload(true);
								console.log(json.message);
							}
							else if (json.reponse === "error") {
								console.log(json.message);
							}
						},
						fail: function() {
							console.log("error on server");
						}
					});
				}
			});

			/**
			 * Get date
			 **/
		 	function getCurrentDate() {
			 	var now = new Date();

			 	var year = now.getFullYear();
			 	var month = ((now.getMonth() + 1) < 10 ? '0' : '') + (now.getMonth() + 1) ;
			 	var day = (now.getDate() < 10 ? '0' : '') + now.getDate();

			 	if($('html').attr('lang') == 'fr') {
					var date = day + '/' + month + '/' + year;
				}
			 	else {
					var date = year + '/' + month + '/' + day;
				}

			 	return date;
			}

			 /**
				* Navigation
			 **/
			 $("#carousel-step1, #carousel-step2, #carousel-step3").on({
				 keydown: function(event) {
         	 if (event.keyCode >= 37 && event.keyCode <= 40) {
           	 event.stopPropagation();
         	 }
			 	 }
    	 });

			 // Next Button Step 1
			 $("#validate-step1-desktop, #validate-step1-mobile").on({
				 click: function() {
					 $('#carousel-wizard').carousel('next');
					 $('#inputHostname').focus();
				 }
			 });

			 // Click when press Enter for Step 1
			 $('#inputWiFiZone, #inputSSID, #inputWiFiKey').on({
				 keypress: function(event) {
 			 	   if (event.which == 13 || event.keyCode == 13) {
						 if(validateStep1() == true) {
							 $("#validate-step1-desktop, #validate-step1-mobile").click();
							 event.preventDefault();
						 }
					 }
				 }
			 });

			 // Previous Button Step 2
			 $("#previous-step2-desktop, #previous-step2-mobile").on({
				 click: function() {
					 $('#carousel-wizard').carousel('prev');
				 }
			 });

			 // Finalisation Button Step 2 (to display modal)
			 $("#display-modal-desktop").on({
				 click: function() {
						 $('#resultWiFiZone').text($('#inputWiFiZone').val());
						 $('#resultSSID').text($('#inputSSID').val());
						 $('#resultWiFiKey').text($('#inputWiFiKey').val());
						 $('#resultHostname').text($('#inputHostname').val());
						 $('#resultPassword').text($('#inputPassword').val());
						 var progress_line = $(this).parents('.f1').find('.f1-progress-line');
				 }
			 });

			 // On click on cancel modal button (desktop), hide modal
			 $('#cancelConfig-modal').on({
			 	click: function() {
			 		$('#squarespaceModalDesktop').modal('hide');
			 	}
			 });

			 // Finalisation Button Step 2 Mobile Version (to display modal)
			 $('#display-modal-mobile').on({
				 click: function() {
					 $('#resultWiFiZoneMobile').text($('#inputWiFiZone').val());
					 $('#resultSSIDMobile').text($('#inputSSID').val());
					 $('#resultWiFiKeyMobile').text($('#inputWiFiKey').val());
					 $('#resultHostnameMobile').text($('#inputHostname').val());
					 $('#resultPasswordMobile').text($('#inputPassword').val());
				 }
			 });

			 // On click on cancel modal button (mobile), hide modal
			 $('#delImage').on({
			 	click: function() {
			 		$('#squarespaceModalMobile').modal('hide');
			 	}
			 });

			 // Click when press Enter for step 2
			 $('#inputHostname, #inputPassword').on({
				 keypress: function(event) {
 			 	   if (event.which == 13 || event.keyCode == 13) {
						 if(validateStep1() == true) {
							 $("#display-modal-desktop, #display-modal-mobile").click();
							 event.preventDefault();
						 }
 			 		 }
				 }
			 });

			// function to setTimeout on Carousel->next
			function NextCarouselWizard() {
				$('#carousel-wizard').carousel('next');
			}

			// Apply config Desktop Version (in modal)
			$("#applyConfig-modal").on({
				click: function() {
					 $("body").append("<div class='mask' style='display: none;'></div>");
					 $(".mask").show() // Lancement du loading
					 $('#spinner-load').show()
					 		var count = $(('#count')); // lancement du compteur
							$({ Counter: 0 }).animate({ Counter: count.text() }, {
  						duration: 20000,
  						easing: 'linear',
  						step: function () {
    					count.text(Math.ceil(this.Counter)+ "%");
 							 }
						});
					$.ajax({
						url: "processing/wizard-cgi.php",
						type: "post",
						data: $("#formWizard").serialize(),
						dataType: 'json',
						success: function(json) {
							if(json.reponse === 'ok') {
								$('#squarespaceModalDesktop').modal('hide');
								$('.modal-backdrop').addClass('hidden');
								$("#spinner-load").delay(20000).fadeOut("slow");
								$(".mask").delay(20000).fadeOut("slow");
								setTimeout(NextCarouselWizard, 20200);
							}
							else {
								$('#configError').text(json.reponse);
							}
						},
						fail: function() {
							console.log("error on server");
						}
					});
				}
			});


			// Apply config Mobile Version (in modal)
			$("#applyConfig-modal-mobile").on({
				click: function() {
					 $("body").append("<div class='mask' style='display: none;'></div>");
					 $(".mask").show()
					 $('#spinner-load').show()
					 		var count = $(('#count')); // lancement du compteur
							$({ Counter: 0 }).animate({ Counter: count.text() }, {
  						duration: 20000,
  						easing: 'linear',
  						step: function () {
    					count.text(Math.ceil(this.Counter)+ "%");
 							 }
						});
					$.ajax({
						url: "processing/wizard-cgi.php",
						type: "post",
						data: $("#formWizard").serialize(),
						dataType: 'json',
						success: function(json) {
							if(json.reponse === 'ok') {
								$('#squarespaceModalMobile').modal('hide');
								$('.modal-backdrop').addClass('hidden');
								$("#spinner-load").delay(20000).fadeOut("slow");
								$(".mask").delay(20000).fadeOut("slow");
								setTimeout(NextCarouselWizard, 20200);
							}
							else {
								$('#configErrorMobile').text(json.reponse);
							}
						},
						fail: function() {
							console.log("error on server");
						}
					});
				}
			});



			/**
			 * Fields checking
			 **/
			// Check Wi-Fi zone
			$("#inputWiFiZone").on({
				change: function() {
					validateStep1();
				}
			});

			// Check SSID, Wi-Fi key
			$("#inputSSID, #inputWiFiKey").on({
				keyup: function() {
					validateStep1();
				}
			});

			// Check Hostname, Password
			$("#inputHostname, #inputPassword").on({
				keyup: function() {
					validateStep2();
				}
			});

			/**
			 * Checking functions
			 **/
			// Check input length
			function checkLength(inputId, fieldLength) {
				if (inputId.val().length < fieldLength) {
					return false;
				}
				else {
					return true;
				}
			}

			// Validate first step
			function validateStep1() {
				var error = 0;

				if ($('#inputWiFiZone option:selected').val() == '' ) {
					error++
				}

				if (checkLength($("#inputSSID"), 1) == false) {
					error++;
				}

				if  (checkLength($("#inputWiFiKey"), 8) == false) {
					error++;
				}

				if (!error) {
					$("#validate-step1-desktop, #validate-step1-mobile").removeAttr('disabled');
					return true;
				}
				else {
					$("#validate-step1-desktop, #validate-step1-mobile").attr('disabled', 'true');
					return false;
				}
			}

			// Validate second step
			function validateStep2() {
				var error = 0;

				if (checkLength($("#inputHostname"), 1) == false) {
					error++;
				}

				if (checkLength($("#inputPassword"), 8) == false) {
					error++;
				}

				if (!error) {
					$("#display-modal-desktop, #display-modal-mobile").removeAttr('disabled');
					return true;
				}
				else {
					$("#display-modal-desktop, #display-modal-mobile").attr('disabled', 'true');
					return false;
				}
			}

			/**
			 * Lastest screens
			 **/
			// Set summary body
			function prepareBodies() {
				var body_sms_email = "";
				var body_pdf = "";

				body_sms_email += "Résumé de votre configuration :\n";

				body_pdf += "WiFi-Zone : " + $("#inputWiFiZone option:selected").text() + "%0a";
				body_pdf += "SSID : " + $("#inputSSID").val() + "%0a";
				body_pdf += "WiFi-Key : " + $("#inputWiFiKey").val() + "%0a";
				body_pdf += "Hostname : " + $("#inputHostname").val() + "%0a";
				body_pdf += "Password : " + $("#inputPassword").val() + "%0a";
				body_pdf += "Date : " + getCurrentDate();

				body_sms_email += body_pdf;

				return { sms_email: body_sms_email, pdf: body_pdf };
			}

			// Create pdf
			function createPDF(bodyPDF, handleData) {
				$.ajax({
					url: "processing/createPDF.php",
					type: "get",
					data: "data=" + bodyPDF + "&" + $("#formWizard").serialize(),
					dataType: 'json',
					success: function(json) {
						if(json.status === 'ok') {
							handleData(json.pdf);
						}
					},
					fail: function() {
						console.log("error on server");
					}
				});
			}

			// Fill last screen
			$('#carousel-wizard').on('slid.bs.carousel', function() {
				var carouselData = $(this).data('bs.carousel');
				var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
				if ( currentIndex == 2 ) {
					// $("#resultWiFiZone").text($("#inputWiFiZone option:selected").text());
					// $("#resultSSID").text($("#inputSSID").val());
					// $("#resultWiFiKey").text($("#inputWiFiKey").val());
					// $("#resultHostname").text($("#inputHostname").val());
					// $("#resultPassword").text($("#inputPassword").val());

			// 		~ $('.wizard_step4_button_left').css('height', $('.wizard_step4_block').height());
			// 		~ $('.wizard_step4_button_middle').css('height', $('.wizard_step4_block').height());
			// 		~ $('.wizard_step4_button_right').css('height', $('.wizard_step4_block').height());
			// 		~ $('.wizard_step4_button_child').css('height', $('.wizard_step4_block').height());

					var bodies = prepareBodies();
					body_sms_email = bodies.sms_email;

			// 		Create pdf
					createPDF(bodies.pdf, function(pdfPath) {
						var pdf = new Image();
						pdf.src = "pdf/" + pdfPath + ".pdf";

						$("#savePDF-lg-md, #savePDF-sm, #savePDF-xs").on({
							click: function() {
								window.open("pdf/" + pdfPath + ".pdf", "_blank");
								// The pdf is saved in pdf/ folder but window.open dosen't work for this time :(
							}
						});
					});

					$("#sendEmail-lg-md, #sendEmail-sm, #sendEmail-xs").on({
						click: function() {
							window.open("mailto:?subject=Votre configuration&body=" + body_sms_email, "_self");
						}
					});

					$("#sendSMS-sm, #sendSMS-xs").on({
						click: function() {
							window.open("sms:?body=" + body_sms_email, "_self");
						}
					});
				}
			});
		</script>
	</body>
</html>
