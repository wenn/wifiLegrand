��          �      |      �  
   �  	   �  	     *     -   ;     i  :   }  @   �  >   �  3   8     l  ,   �     �     �  -   �               2     E     d  A   �  A  �            	     .   $     S     j  I   �  M   �  M        f       &   �     �  )   �  *   �                8     S     p  F   �            
                                             	                                        Adresse IP Appliquer BIENVENUE Configuration IP de l'interface de gestion Fichier de micrologiciel (firmware) incorrect Gestion du firmware L'envoi du fichier a été interrompu pendant le transfert Le fichier dépasse la limite autorisée dans le formulaire HTML Le fichier fourni dépasse la limite autorisée par le produit Le fichier que vous avez envoyé a une taille nulle Masque de sous-réseau Mettre le firmware à jour depuis un fichier Mettre à jour Mode d'adressage IP Obtenir une adresse IP automatiquement (DHCP) Passerelle par défaut Selectionnez votre langue Transfert échoué Utiliser l'adresse IP suivante Version actuelle du firmware sur l'interface de configuration du point d'accès Wi-Fi Legrand. Project-Id-Version: Wi-Fi Soho 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-24 07:30+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Indirizzo IP Applica BENVENUTO Configurazione IP dell'interfaccia di gestione Firmware file invalido Gestione del firmware Il trasferimento del file è stato interrotto prima del suo completamento Il file fornito supera la dimensione massima consentita in questo modulo HTML Il file fornito supera la dimensione massima consentita su questo dispositivo Il file inviato è vuoto Netmask Aggiornamento del firmware con il file Aggiorna Indirizzamento IP per la gestione dell'AP Ricerca dinamica di un indirizzo IP (DHCP) Gateway predefinito Seleziona la sua lingua Trasferimento non riuscito Usa il seguente indirizzo IP Versione corrente del firmware sull'interfaccia di configurazione del punto di accesso Wi-Fi Legrand. 