��          �      |      �  
   �  	   �  	     *     -   ;     i  :   }  @   �  >   �  3   8     l  ,   �     �     �  -   �               2     E     d  A   �  A  �            
     ,   &     S     r  D   �  _   �  [   2     �     �  *   �  
   �  +   �  6         W     l  ,   �  #   �  !   �  9   �            
                                             	                                        Adresse IP Appliquer BIENVENUE Configuration IP de l'interface de gestion Fichier de micrologiciel (firmware) incorrect Gestion du firmware L'envoi du fichier a été interrompu pendant le transfert Le fichier dépasse la limite autorisée dans le formulaire HTML Le fichier fourni dépasse la limite autorisée par le produit Le fichier que vous avez envoyé a une taille nulle Masque de sous-réseau Mettre le firmware à jour depuis un fichier Mettre à jour Mode d'adressage IP Obtenir une adresse IP automatiquement (DHCP) Passerelle par défaut Selectionnez votre langue Transfert échoué Utiliser l'adresse IP suivante Version actuelle du firmware sur l'interface de configuration du point d'accès Wi-Fi Legrand. Project-Id-Version: Wi-Fi Soho 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-24 07:30+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Dirección IP Aplicar BINEVENIDO Configuración IP de la interfaz de gestión Archivo de firmware no válido Gestión del microprograma Se ha interrumpido la transferencia del archivo antes de completarse El tamaño del archivo suministrado supera el tamaño máximo permitido en este formulario HTML El tamaño del archivo suministrado supera el tamaño máximo permitido en este dispositivo El archivo enviado está vacío Máscara de red Actualizar el microprograma con un archivo Actualizar Direccionamiento IP para la gestión del PA Recuperar una dirección IP de manera dinámica (DHCP) Pasarela por defecto Seleccione un idioma Se ha producido un fallo en la transferencia Utilizar la siguiente dirección IP Versión actual del microprograma on the setup interface of the Legrand Wi-Fi access point. 