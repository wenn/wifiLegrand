<?php
require_once('utils.php');
$status = "error";
$message = "";

// if (isset($_POST['announcing_protocols_status']) && !empty($_POST['announcing_protocols_status'])) {
// 	file_put_contents("/tmp/phplog",  "announcing-protocols-cgi.php: " . "Params are set and not empty" . "\n", FILE_APPEND);
// 	$ret = setAnnouncingProtocolsStatus($_POST["announcing_protocols_status"]);
// 	file_put_contents("/tmp/phplog",  "announcing-protocols-cgi.php: " . "Call performed" . "\n", FILE_APPEND);
// 	if (!$ret) {
// 		$message = "Cannot change Announcing Protocols status to ". $_POST["announcing_protocols_status"] .".";
// 	}
// 	else {
// 		$status = "ok";
// 		$message = "Announcing Protocols status successfully changed.";
// 	}
// }
// else {
// 	$message = "No Announcing Protocols status has been specified";
// }
//
// file_put_contents("/tmp/phplog",  "announcing-protocols-cgi.php: " . json_encode(['status' => $status, 'message' => $message, "announcing_protocols_status"=>$_POST["announcing_protocols_status"]]) . "\n", FILE_APPEND);
// echo json_encode(['status' => $status, 'message' => $message, "announcing_protocols_status"=>$_POST["announcing_protocols_status"]]);

$status = 'error';
$message = '';

// Basic check for inputs
if (isset($_POST['announcing_protocols_status']) && !empty($_POST['announcing_protocols_status'])) {
	$setStatus = setAnnouncingProtocolsStatus($_POST['announcing_protocols_status']); // try to set Protocol status in json file
	if ($setStatus == false) {
		$message = "Cannot change Announcing Protocols status to ". $_POST["announcing_protocols_status"] ."."; // error
	}
	else {
		$status = "ok";
		$message = "Announcing Protocols status successfully changed to " . $_POST["announcing_protocols_status"] . '.'; // OK
	}
}
else {
	$message = "No Announcing Protocols status has been specified";
}

echo json_encode(['status' => $status, 'message' => $message, "announcing_protocols_status"=>$_POST["announcing_protocols_status"]]);
?>
