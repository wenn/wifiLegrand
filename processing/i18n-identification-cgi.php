<?php
// Change language for identification.php
session_start();

if (isset($_POST['lang']) && !empty($_POST['lang'])) {

  $_SESSION['lang'] = $_POST['lang'];
  $response = 'ok';
  $message = 'Language succefully changed.';
}
else {
  $response = 'error';
  $message = 'No language specified.';
}

echo json_encode(['reponse' => $response, 'message' => $message]);

?>
