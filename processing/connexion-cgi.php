<?php
session_start();

// require_once('localization.php');
require_once('utils.php');

$response = "";
// $code = exec("getcode");

// Basic check for inputs
if (isset($_POST['code']) && !empty($_POST['code'])) {
		if(strlen($_POST['code']) == 8) { // if code length == 8 & code == realCode, write users-infos in SESSION
			// if ($_POST['code'] == $code) {
				$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
				$_SESSION['user-agent'] = $_SERVER['HTTP_USER_AGENT'];
				$_SESSION['identifier'] = $_POST['code'];
				$_SESSION['lang'] = isset($_POST['lang']) ? $_POST['lang'] : '';
				$response = 'ok';
				// header("Location: ../wizard.php");
			// }
			// else {
			// 	$response = _("Code erroné");
			// }
		}
		else {
			$response = 'The identifier must contain 8 characters.';
		}
	// }
	// else {
	//   $response = __("Code erroné");
	// }
}
else {
	$response = '';
}

echo json_encode(['reponse' => $response]);
?>
