<?php

class JsonRequest {

  private $_folder;

  public function __construct($folder) {
    $this->_folder = $folder;
  }

  // function to create configuration
  public function create($fileName, $content) {
    if (file_exists($this->_folder . $fileName)) {
      return false;
    }
    else {
      $fp = fopen($this->_folder . $fileName, 'x');
      $json = json_encode($content);
      fwrite($fp, $json);
      fclose($fp);
      return true;
    }
  }

  // function to read configuration file
  public function read($fileName) {
    if (file_exists($this->_folder . $fileName)) {
      $jsonSource = file_get_contents($this->_folder . $fileName);
      $jsonData = json_decode($jsonSource, true);
      return $jsonData;
    }
    else {
      return false;
    }
  }
}

?>
