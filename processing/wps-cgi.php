<?php
require_once('utils.php');
$status = "error";
$message = "";

// if (isset($_POST['upnp_status']) && !empty($_POST['upnp_status'])) {
// 	file_put_contents("/tmp/phplog",  "upnp-cgi.php: " . "Will call with parameter " . $_POST['upnp_status'] . "\n", FILE_APPEND);
// 	$ret = setUPnPStatus($_POST["upnp_status"]);
// 	file_put_contents("/tmp/phplog",  "upnp-cgi.php: " . "SetUPnPStatus ended\n", FILE_APPEND);
// 	if (!$ret) {
// 		$message = "Cannot change UPnP Announcing status to ". $_POST["upnp_status"] .".";
// 	}
// 	else {
// 		$status = "ok";
// 		$message = "UPnP Announcing status successfully changed.";
// 	}
// }
// else {
// 	file_put_contents("/tmp/phplog",  "upnp-cgi.php: " . "UPnP Status not set or empty \n", FILE_APPEND);
// 	$message = "No UPnP Announcing status has been specified";
// }
//
// file_put_contents("/tmp/phplog",  "upnp-cgi.php: " . json_encode(['status' => $status, 'message' => $message, "upnp_status"=>$_POST["upnp_status"]]) . "\n", FILE_APPEND);
// echo json_encode(['status' => $status, 'message' => $message, "upnp_status"=>$_POST["upnp_status"]]);
//
// }

$status = 'error';
$message = '';

// Basic check inputs
if (isset($_POST['wpsStatus']) && !empty($_POST['wpsStatus'])) {
	$setStatus = setWpsStatus($_POST['wpsStatus']); // try to set wps status in json file
	if ($setStatus == false) {
		$message = "Cannot change WPS status to ". $_POST["wpsStatus"] ."."; // error
	}
	else {
		$status = "ok";
		$message = "WPS status successfully changed to " . $_POST['wpsStatus'] . '.'; // OK
	}
}
else {
	$message = "No WPS status has been specified";
}

echo json_encode(['status' => $status, 'message' => $message, "wps_status"=>$_POST["wpsStatus"]]);
?>
