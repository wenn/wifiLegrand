<?php
require_once('utils.php');

/**
 * \fn checkIPV4Address($ip_addr_1, $ip_addr_2, $ip_addr_3, $ip_addr_4)
 * \brief Check if an ipv4 address is valid
 *
 * \param $ip_addr_1 First field of the ip adress
 * \param $ip_addr_2 Second field of the ip adress
 * \param $ip_addr_3 Third field of the ip adress
 * \param $ip_addr_4 Fourth field of the ip adress
 *
 * \return Nothing, raises an exception if there is a problem
 */
function checkIPV4Address($ip_addr_1, $ip_addr_2, $ip_addr_3, $ip_addr_4) {
	if ( !is_numeric($ip_addr_1) || !is_numeric($ip_addr_2) || !is_numeric($ip_addr_3) || !is_numeric($ip_addr_4)
		|| $ip_addr_1<1 || $ip_addr_1>239 || $ip_addr_2<0 || $ip_addr_2>254
		|| $ip_addr_3<0 || $ip_addr_3>254 || $ip_addr_4<1 || $ip_addr_4>254 )  {
		//echo "Adresse IP : ".$ip_addr_1.".".$ip_addr_2.".".$ip_addr_3.".".$ip_addr_4;	//DEBUG

		throw new Exception("Adresse IP invalide");			//Throw new exception
	}
}


/**
 * \fn checkNetmaskFieldIPV4($mask)
 * \brief Check wether a netmask field is correct or not, used in checkNetmaskIPV4
 *
 * \param $mask Netmask's field
 *
 * \return Nothing, raises an exception if there is a problem
 */
function checkNetmaskFieldIPV4($mask) {
	switch($mask) {
		case 255:
		case 254:
		case 252:
		case 248:
		case 240:
		case 224:
		case 192:
		case 128:
		case 0:
			return true;
			break;
	}

	return false;
}

/*
Function to check if the netmask address is correct independently
*/
/**
 * \fn checkNetmaskIPV4($netmask_1, $netmask_2, $netmask_3, $netmask_4)
 * \brief Check if the netmask address is correct independently
 *
 * \param $netmask_1 Netmask's first field
 * \param $netmask_2 Netmask's second field
 * \param $netmask_3 Netmask's third field
 * \param $netmask_4 Netmask's fourth field
 *
 * \return Nothing, raises an exception if there is a problem
 */
function checkNetmaskIPV4($netmask_1, $netmask_2, $netmask_3, $netmask_4) {
	//Check each field independently
	if (!is_numeric($netmask_1) || !is_numeric($netmask_2) || !is_numeric($netmask_3) || !is_numeric($netmask_4)
		|| !checkNetmaskFieldIPV4($netmask_1) || !checkNetmaskFieldIPV4($netmask_2) || !checkNetmaskFieldIPV4($netmask_3) || !checkNetmaskFieldIPV4($netmask_4)
		|| $netmask_1 < 28 || $netmask_4 > 252 ) {
		throw new Exception("Adresse Netmask invalide");			//Throw new exception
	}
	else {
		//Get the binary representation of each fieldof the netmask
		//We must check that when we find the first 0
		//Then everything after must be a 0 too
		$netmask_1_bin = decbin($netmask_1);
		$netmask_2_bin = decbin($netmask_2);
		$netmask_3_bin = decbin($netmask_3);
		$netmask_4_bin = decbin($netmask_4);

		$zerofound = 0;		//Var used to know if we found a '0' or not
		$prefix = 0;			//Var used to calculate current prefix

		//loop on the 4 dynamic variables netmask_.$j._bin
		//each one is the binary representation of a field of the netmask address
		//enable us to write only one "while" instead of 4
		for ($field=1; $field<=4; $field++){

			//Debug
			//echo "variable : ".${'netmask_'.$field.'_bin'}."<br/>";

			$cur_netmask_bin = ${'netmask_'.$field.'_bin'};	// Get the current byte of the netmask (1st, 2nd, 3rd, then 4th)

			//bitno = 0 means we are checking the MSB (bit order 7, value 128)
			//bitno = 7 means we are checking the LSB (bit order 0, value 1)
			$bitno = 0;								//loop variable

			//we'll parse every bit of the field
			//If a '0' has already been found before and we find a '1', the netmask is invalid
			while ($bitno < strlen($cur_netmask_bin)) {

				if ($cur_netmask_bin[$bitno] == 0 && !$zerofound) {

					$zerofound =1;					//it's the first time we find a '0', everything after should be a zero
				}

				if ($cur_netmask_bin[$bitno] == 1 && $zerofound) {	//we've found a '1' after finding a '0', the address is invalid

					throw new Exception("Adresse Netmask invalide");			//Throw new exception
				}

				if (!$zerofound) {
					$prefix++;						//calculate the prefix
				}

				$bitno++;							//Get to the next bit
			}
		}
	}
}


/**
 * \fn checkGatewayIPV4($gateway_1, $gateway_2, $gateway_3, $gateway_4)
 * \brief Check if the gateway address is correct independently
 *
 * \param $gateway_1 First field of gateway address
 * \param $gateway_2 Second field of gateway address
 * \param $gateway_3 Third field of gateway address
 * \param $gateway_4 Fourth field of gateway address
 *
 * \return Nothing, raises an exception if there is a problem
 */
function checkGatewayIPV4($gateway_1, $gateway_2, $gateway_3, $gateway_4) {
	if ( !(is_numeric($gateway_1)) || (!is_numeric($gateway_2)) || !(is_numeric($gateway_3)) || !(is_numeric($gateway_4))
		|| $gateway_1<1 || $gateway_1>239 || $gateway_2<0 || $gateway_2>254
		|| $gateway_3<0 || $gateway_3>254 || $gateway_4<1 || $gateway_4>254 )
	{
		throw new Exception("Adresse Gateway invalide");			//Throw new exception
	}
}


/**
 * \fn checkConfigurationIPV4($ip_addr_1, $ip_addr_2, $ip_addr_3, $ip_addr_4, $netmask_1, $netmask_2, $netmask_3, $netmask_4, $gateway_1, $gateway_2, $gateway_3, $gateway_4)
 * \brief Check if the whole configuration is correct
 *
 * \param $ip_addr_1 First field of ip address
 * \param $ip_addr_2 Second field of ip address
 * \param $ip_addr_3 Third field of ip address
 * \param $ip_addr_4 Fourth field of ip address
 * \param $netmask_1 First field of netmask address
 * \param $netmask_2 Second field of netmask address
 * \param $netmask_3 Third field of netmask address
 * \param $netmask_4 Fourth field of netmask address
 * \param $gateway_1 First field of gateway address
 * \param $gateway_2 Second field of gateway address
 * \param $gateway_3 Third field of gateway address
 * \param $gateway_4 Fourth field of gateway address
 *
 * \return Nothing, raises an exception if there is a problem
 */
function checkConfigurationIPV4($ip_addr_1, $ip_addr_2, $ip_addr_3, $ip_addr_4, $netmask_1, $netmask_2, $netmask_3, $netmask_4, $gateway_1, $gateway_2, $gateway_3, $gateway_4) {

	//IP Address mustn't be the subnetwork address
	//Applying netmask on it, if it doesn't change, the address is the network address
	if ( (($ip_addr_1 & $netmask_1) == $ip_addr_1) &&
		(($ip_addr_2 & $netmask_2) == $ip_addr_2) &&
		(($ip_addr_3 & $netmask_3) == $ip_addr_3) &&
		(($ip_addr_4 & $netmask_4) == $ip_addr_4) ) {

		throw new Exception("L'adresse IP ne correspond pas à un hôte, mais à l'adresse du sous-réseau");
	}

	//IP Address mustn't be the broadcast address
	if ( (($ip_addr_1 | ($netmask_1 ^ 255)) == $ip_addr_1) &&
	     (($ip_addr_2 | ($netmask_2 ^ 255)) == $ip_addr_2) &&
	     (($ip_addr_3 | ($netmask_3 ^ 255)) == $ip_addr_3) &&
	     (($ip_addr_4 | ($netmask_4 ^ 255)) == $ip_addr_4) ) {

		throw new Exception("L'adresse IP est l'adresse de broadcast du sous-réseau");
	}

	//throw new Exception("gate1 :". ($ip_addr_1 & $netmask_1) . "!=". ($gateway_1 & $netmask_1). "\ngate2 :" . ($ip_addr_2 & $netmask_2)."!=".($gateway_2 & $netmask_2)."\ngate3 :".($ip_addr_3 & $netmask_3)."!=".($gateway_3 & $netmask_3)."\ngate4 :".($ip_addr_4 & $netmask_4)."!=".($gateway_4 & $netmask_4)."\n");

	//Gateway must be in the same subnetwork as the IP  Address (if there is a gateway)
	//Applying netmask on both addresses to check if the result is equal
	if (!is_null($gateway_1) && !is_null($gateway_2) && !is_null($gateway_3) && !is_null($gateway_4)) {
		if ( (($ip_addr_1 & $netmask_1) != ($gateway_1 & $netmask_1)) ||
		     (($ip_addr_2 & $netmask_2) != ($gateway_2 & $netmask_2)) ||
		     (($ip_addr_3 & $netmask_3) != ($gateway_3 & $netmask_3)) ||
		     (($ip_addr_4 & $netmask_4) != ($gateway_4 & $netmask_4)) ) {

			throw new Exception("La gateway ne se situe pas dans le même sous-réseau");
		}
	}
}

$response = "";
$status = "error";
$config = array();

if (!isset($_POST['inputIPtype']) || $_POST['inputIPtype'] == 'AutoIP') {
	$config['IP_address_type'] = 'dhcp';
}
elseif ($_POST['inputIPtype'] == 'StaticIP') {
	$config['IP_address_type'] = 'static';
	try {
		/* Check if the ip address is correct */
		checkIPV4Address($_POST['ip_addr_ipv4_1'], $_POST['ip_addr_ipv4_2'], $_POST['ip_addr_ipv4_3'], $_POST['ip_addr_ipv4_4']);

		/* Check if the netmask is correct */
		checkNetmaskIPV4($_POST['netmask_ipv4_1'], $_POST['netmask_ipv4_2'], $_POST['netmask_ipv4_3'], $_POST['netmask_ipv4_4']);

		/* Check if the gateway has been set to NULL */
		if($_POST['gateway_ipv4_1'] != "" && $_POST['gateway_ipv4_2'] != "" && $_POST['gateway_ipv4_3'] != "" && $_POST['gateway_ipv4_4'] != "") {
			/* Check if the gateway is correct */
			checkGatewayIPV4($_POST['gateway_ipv4_1'], $_POST['gateway_ipv4_2'], $_POST['gateway_ipv4_3'], $_POST['gateway_ipv4_4']);
		}

		if($_POST['dns_1'] != "" && $_POST['dns_2'] != "" && $_POST['dns_3'] != "" && $_POST['dns_4'] != "") {
			/* Check if the dns address is correct */
			checkIPV4Address($_POST['dns_1'], $_POST['dns_2'], $_POST['dns_3'],$_POST['dns_4']);
		}

		/* Check if the whole configuration is correct */
		checkConfigurationIPV4(intval($_POST['ip_addr_ipv4_1']), intval($_POST['ip_addr_ipv4_2']), intval($_POST['ip_addr_ipv4_3']), intval($_POST['ip_addr_ipv4_4']),
					      intval($_POST['netmask_ipv4_1']), intval($_POST['netmask_ipv4_2']), intval($_POST['netmask_ipv4_3']), intval($_POST['netmask_ipv4_4']),
					      intval($_POST['gateway_ipv4_1']), intval($_POST['gateway_ipv4_2']), intval($_POST['gateway_ipv4_3']), intval($_POST['gateway_ipv4_4']));

	}
	catch (Exception $e) {
		$response = $response . $e->getMessage();
	}
}
else {
	$response .= "Invalid IP method.";
}

// Check if the Canal is defined
if (isset($_POST['canalChoice']) && !empty($_POST['canalChoice'])) {
	$config['canal'] = $_POST['canalChoice'];
}
else {
	$response = 'No Canal defined.';
}

// Check if Encryption type is defined
if (isset($_POST['cryptChoice']) && !empty($_POST['cryptChoice'])) {
	$config['Encryption_type'] = $_POST['cryptChoice'];
}
else {
	$response = 'No Encryption defined.';
}

// Check if SSID is defined
if (isset($_POST['SSID']) && !empty($_POST['SSID'])) {
	$config['SSID'] = $_POST['SSID'];
}
else {
	$response = 'No SSID defined.';
}

// Check if Regulatory domain is defined
if (isset($_POST['domain']) && !empty($_POST['domain'])) {
	$config['Regulatory_domain'] = $_POST['domain'];
}
else {
	$response = 'No Regulatory domain defined.';
}

// Check if Regulatory domain is defined
if (isset($_POST['access_point']) && !empty($_POST['access_point'])) {
	$config['Device_name'] = $_POST['access_point'];
}
else {
	$response = 'No Access point defined.';
}

// Check the wifi key, if it set not correctly, destroy it
if (isset($_POST['WiFiKey']) && !empty($_POST['WiFiKey'])) {
	if (!preg_match("#^[A-Za-z0-9]+$#", $_POST['WiFiKey'])) {
		if ($_POST['WiFiKey'] === '****************') {
			$config['WiFiKey'] = '';
		}
		else {
			$response = 'WiFi key can\'t contain spécials characters.';
		}
	}
	else {
		if (strlen($_POST['WiFiKey']) >= 8) {
			if (preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])#", $_POST['WiFiKey'])) {
	  		$config['WiFiKey'] = $_POST['WiFiKey'];
			}
			else {
				$response = 'WiFi key must contain numbers and letters (LowerCase and UpperCase).';
			}
		}
		else {
			$response = 'WiFi key must contain minimum 8 characters.';
		}
	}
}

// Check if the modification of password is set
if (isset($_POST['inputOldPassword']) && isset($_POST['inputNewPassword']) && isset($_POST['inputNewPassword2'])) {
	// Oldpassword empty ?
	if (!empty($_POST['inputOldPassword'])) {
		// New passwords empty ?
		if (!empty($_POST['inputNewPassword']) && !empty($_POST['inputNewPassword2'])) {
			// Check length of new passwords
			if (strlen($_POST['inputNewPassword']) > 7 && strlen($_POST['inputNewPassword2']) > 7) {

				// Check if configuration file exist
				$data = directoryStatus('../wizardData/data/');
	  		if($data === 'Exist') {
	    		$dir = '../wizardData/data/';
	    		$file = scandir($dir, 1);

	    		// Don't open index.php
	    		$jsonSource = new JsonRequest('../wizardData/data/');
	    		if ($file[0] === "index.php") {
	     			$jsonData = $jsonSource->read($file[1]);
	    		}
	    		else {
	      		$jsonData = $jsonSource->read($file[0]);
	    		}

	    		// If old password == password in configuration file
	    		if(password_verify($_POST['inputOldPassword'], $jsonData['Admin_password']) == true) {

	    			// if the new passwords are the same
	    			if ($_POST['inputNewPassword'] === $_POST['inputNewPassword2']) {
	    				// All the verifs is good ! Hash of new password
	    				$passHash = hashPassword($_POST['inputNewPassword']);
	    			}
	    			else {
	    				$response = 'New passwords are not the same.';
	    			}
	    		}
	    		else {
	    			$response = 'Old password is incorrect.';
	    		}
				}
				else {
					$response = 'No existing configuration.';
				}
			}
			else {
				$response = 'Password need to contain 8 or more characters.';
			}
		}
		else {
			$response = 'New password is incorrect.';
		}
	}
}

if (empty($_POST['inputOldPassword']) && !empty($_POST['inputNewPassword'] || $_POST['inputNewPassword2'])) {
	$response = 'Old password is incorrect.';
}

if (empty($response)) {
	try {
		$config['IP_address'] = $_POST['ip_addr_ipv4_1'] . '.' . $_POST['ip_addr_ipv4_2'] . '.' . $_POST['ip_addr_ipv4_3'] . '.' . $_POST['ip_addr_ipv4_4'];
		$config['IP_prefix_length'] = strval(maskToCIDR($_POST['netmask_ipv4_1'] . '.' . $_POST['netmask_ipv4_2'] . '.' . $_POST['netmask_ipv4_3'] . '.' . $_POST['netmask_ipv4_4']));

		$config['Netmask'] = $_POST['netmask_ipv4_1'] . '.' . $_POST['netmask_ipv4_2'] . '.' . $_POST['netmask_ipv4_3'] . '.' . $_POST['netmask_ipv4_4'];

		/* If the gateway has been set to NULL, it will be deleted */
		if( $_POST['gateway_ipv4_1'] == '' || $_POST['gateway_ipv4_2'] == '' || $_POST['gateway_ipv4_3'] == '' || $_POST['gateway_ipv4_4'] == '') {
			$config['IP_default_gateway'] = "";
		}
		else {
			$config['IP_default_gateway'] = $_POST['gateway_ipv4_1'] . '.' . $_POST['gateway_ipv4_2'] . '.' . $_POST['gateway_ipv4_3'] . '.' . $_POST['gateway_ipv4_4'];
		}
		/* If the gateway has been set to NULL, it will be deleted */
		if( $_POST['dns_1'] == '' || $_POST['dns_2'] == '' || $_POST['dns_3'] == '' || $_POST['dns_4'] == '') {
			$config['DNS'] = "";
		}
		else {
			$config['DNS'] = $_POST['dns_1'] . '.' . $_POST['dns_2'] . '.' . $_POST['dns_3'] . '.' . $_POST['dns_4'];
		}

		// $ipConfigForm = new JsonRpcUnixDomainSocketClient('/var/run/ipconfigform.sock');

		// $request = new RpcRequest('setIpConfig', $config);
		// $rpc_response = (array)$ipConfigForm->call($request);
		// $execOk = array_key_exists("result", $rpc_response);
		// if (! $execOk || ! $rpc_response["result"]) {
		// 	throw new Exception("Cannot store configuration");
		// }
		// $status = 'ok';

	  // Write Canal in $config array
	  $config['Canal'] = $_POST['canalChoice'];

	  $data = directoryStatus('../wizardData/data/');
		// Ckeck if configuration file exist
	  if($data === 'Exist') {
	    $dir = '../wizardData/data/';
	    $file = scandir($dir, 1);

	    $jsonSource = new JsonRequest('../wizardData/data/');
	    // If $file[0] is index.php, read the next file
	    if ($file[0] === "index.php") {
	      $jsonData = $jsonSource->read($file[1]);

	      // Write config in Json array
	      $jsonData['Canal'] = $config['Canal'];
	      $jsonData['IP_address_type'] = $config['IP_address_type'];
	      $jsonData['Regulatory_domain'] = $config['Regulatory_domain'];
	      $jsonData['SSID'] = $config['SSID'];
	      $jsonData['Encryption_type'] = $config['Encryption_type'];
	      $jsonData['Device_name'] = $config['Device_name'];

	      // If user select a manual static IP
	      if ($jsonData['IP_address_type'] == 'static') {
	      	// Write manual static IP
	      	$jsonData['IP_address'] = $config['IP_address'];
	      	$jsonData['Netmask'] = $config['Netmask'];
	      	$jsonData['IP_prefix_length'] = $config['IP_prefix_length'];
	      	// if gateway isn't empty, write it
	      	if ($config['IP_default_gateway'] != '') {
	      		$jsonData['IP_default_gateway'] = $config['IP_default_gateway'];
	      	}
	      	// if dns isn't empty, write it
	      	if ($config['DNS'] != '') {
	      		$jsonData['DNS'] = $config['DNS'];
	      	}
	    	}
	    	else { // If user select an auto IP
	    		$jsonData['IP_address'] = $_POST['ip_addr'];
	    		// if a manual config also exist, delete it
	    		if (isset($jsonData['IP_prefix_length']) || isset($jsonData['IP_default_gateway'])) {
	    			unset($jsonData['IP_prefix_length']);
	    			unset($jsonData['IP_default_gateway']);
	    			unset($jsonData['Netmask']);
	    			unset($jsonData['DNS']);
	    		}
	    	}

	    	// if WiFiKey is set
	      	if (isset($config['WiFiKey']) && !empty($config['WiFiKey'])) {
	      		$jsonData['WiFiKey'] = $config['WiFiKey'];
	      	}

	      // if password has been changed
			  if (isset($passHash) && !empty($passHash)) {
			  	// Write passwordHash
			  	$jsonData['Admin_password'] = $passHash;
			  }

			  // json_encode of config file
			  $json = json_encode($jsonData);
			  // Push content of $jsonData in Json file
	      file_put_contents('../wizardData/data/' . $file[1], $json);
	    }
	    else { // If $file[0] != index.php, read this file
	      $jsonData = $jsonSource->read($file[0]);

	      // Write config in Json array
	      $jsonData['Canal'] = $config['Canal'];
	      $jsonData['IP_address_type'] = $config['IP_address_type'];
	      $jsonData['Regulatory_domain'] = $config['Regulatory_domain'];
	      $jsonData['SSID'] = $config['SSID'];
	      $jsonData['Encryption_type'] = $config['Encryption_type'];
	      $jsonData['Device_name'] = $config['Device_name'];

	      // If user select a manual static IP
	      if ($jsonData['IP_address_type'] == 'static') {
	      	// Write manual static IP
	      	$jsonData['IP_address'] = $config['IP_address'];
	      	$jsonData['Netmask'] = $config['Netmask'];
	      	$jsonData['IP_prefix_length'] = $config['IP_prefix_length'];
	      	// if gateway isn't empty, write it
	      	if ($config['IP_default_gateway'] != '') {
	      		$jsonData['IP_default_gateway'] = $config['IP_default_gateway'];
	      	}
	      	// if dns isn't empty, write it
	      	if ($config['DNS'] != '') {
	      		$jsonData['DNS'] = $config['DNS'];
	      	}
	    	}
	    	else { // If user select an auto IP
	    		$jsonData['IP_address'] = $_POST['ip_addr'];
	    		// if a manual config also exist, delete it
	    		if (isset($jsonData['IP_prefix_length']) || isset($jsonData['IP_default_gateway'])) {
	    			unset($jsonData['IP_prefix_length']);
	    			unset($jsonData['IP_default_gateway']);
	    			unset($jsonData['Netmask']);
	    			unset($jsonData['DNS']);
	    		}
	    	}

	    	// if WiFiKey is set
	      	if (isset($config['WiFiKey']) && !empty($config['WiFiKey'])) {
	      		$jsonData['WiFiKey'] = $config['WiFiKey'];
	      	}

	      // if password has been changed
			  if (isset($passHash) && !empty($passHash)) {
			  	// Write passwordHash
			  	$jsonData['Admin_password'] = $passHash;
			  }

			  // json_encode of config file
			  $json = json_encode($jsonData);
			  // Push content of $jsonData in Json file
	      file_put_contents('../wizardData/data/' . $file[0], $json);
	    }
	  }
	  $status = 'ok';
	}
	catch (Exception $e) {
		$response = $e->getMessage();
	}
}

header('Content-Type: application/json');
echo json_encode(['status' => $status, 'message' => $response]);
?>
