<?php
session_start();

require_once('utils.php');

// $wizardForm = new JsonRpcUnixDomainSocketClient('/var/run/wizardform.sock');
$wizardForm = new JsonRequest('../wizardData/data/');

$response = "";

// Basic check for inputs
if (isset($_POST['inputWiFiZone'], $_POST['inputSSID'], $_POST['inputWiFiKey'], $_POST['inputHostname'], $_POST['inputPassword'])) {
	if ( empty($_POST['inputWiFiZone']) ) {
		$response = $response . "No Wi-Fi zone defined\n";
	}

	if ( empty($_POST['inputSSID']) ) {
		$response = $response . "No SSID defined\n";
	}

	if ( empty($_POST['inputWiFiKey']) ) {
		$response = $response . "No Wi-Fi key defined\n";
	}

	if ( empty($_POST['inputHostname']) ) {
		$response = $response . "No hostname defined\n";
	}

	if ( empty($_POST['inputPassword']) ) {
		$response = $response . "No password defined\n";
	}

	if ( empty($response) ) {
		try {
			// Calculate key
			// $key = "";
			// exec('wpa_passphrase "' . $_POST['inputSSID'] . '" "' . $_POST['inputWiFiKey'] . '"', $wpa_key);
			// foreach($wpa_key as $line){
			// 	if ( preg_match('/^[[:blank:]]+psk=([a-f0-9]+)$/', $line, $key) ) {
			// 		break;
			// 	}
			// }
			// if (! $key[1]) {
			// 	throw new Exception("Can't calculate psk");
			// }

			// Calculate password & identifier
			// $password = crypt(trim($_POST['inputPassword']), '$1$$');

			// crypt identifier and password
			$identifierHash = hashPassword($_SESSION['identifier']);
			$passHash = hashPassword($_POST['inputPassword']);

			// write config in $config array
			$config = array();
			$config['IP_user'] = $_SESSION['ip'];
			$config['Language'] = $_POST['languageOption'];
			$config['User-agent'] = $_SESSION['user-agent'];
			$config['Identifier'] = $identifierHash;
			$config['SSID'] = $_POST['inputSSID'];
			$config['Encryption_type'] = 'wpa-or-802.11i-psk';
			$config['WiFiKey'] = $_POST['inputWiFiKey'];
			$config['Key_format'] = '';
			// $config['Key'] = $key[1];
			$config['Regulatory_domain'] = $_POST['inputWiFiZone'];
			$config['Device_name'] = $_POST['inputHostname'];
			$config['Admin_password'] = $passHash;

			// Give me the date please ? okay.
			$today = getdate();
			$date = array('day' => $today['mday'], 'month' => $today['month'], 'year' => $today['year']);
			$config['Date'] = $date; // Thanks !!

			// $request = new RpcRequest('setWizard', $config);
			// $rpc_response = (array)$wizardForm->call($request);
			// $execOk = array_key_exists("result", $rpc_response);
			// if (! $execOk || ! $rpc_response["result"]) {
			// 	throw new Exception("Cannot store configuration");
			// }
			// $response = 'ok';

			// Create json file with all the config
			$jsonData = $wizardForm->create($_POST['inputHostname'], $config);
			$jsonData == false ? $response = 'Cannot store configuration, also exist' : $response = 'ok';

		}
		catch (Exception $e) {
			$response = $e->getMessage();
		}
	}
}

echo json_encode(['reponse' => $response]);

$_SESSION = array();
session_destroy();
?>
