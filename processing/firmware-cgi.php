<?php
require_once('localization.php');

$status = "error";
$message = "";

try {
	switch ($_FILES['fw_image']['error']) {
		case UPLOAD_ERR_OK:
			break;
		case UPLOAD_ERR_INI_SIZE:
			throw new RuntimeException(_("Le fichier fourni dépasse la limite autorisée par le produit"));
			//echo(lz('__0334__'));
		case UPLOAD_ERR_FORM_SIZE:
			throw new RuntimeException(_("Le fichier dépasse la limite autorisée dans le formulaire HTML"));
			//echo(lz('__0335__'));
		case UPLOAD_ERR_PARTIAL:
			throw new RuntimeException(_("L'envoi du fichier a été interrompu pendant le transfert"));
			//echo(lz('__0336__'));
		case UPLOAD_ERR_NO_FILE:
			throw new RuntimeException(_("Le fichier que vous avez envoyé a une taille nulle"));
			//echo(lz('__0337__'));
		default:
			throw new RuntimeException('Unknown error');
	}

	if (! move_uploaded_file($_FILES['fw_image']['tmp_name'], '/tmp/fw_image.bin')) {
		throw new RuntimeException(_("Transfert échoué"));
		//echo(lz('__0338__'));
	}
	$status = "ok";
}
catch (RuntimeException $e) {
	$message = $e->getMessage();
}

header('Content-Type: application/json');
echo json_encode(['status' => $status, 'message' => $message]);
?>
