<?php
require_once('i18n.php');

$status = "error";
$message = "";

try {
	$sock = fsockopen("unix:///var/run/update_image_fb.sock", -1, $errno, $errstr, 30);
	if (!$sock) {
		throw new RuntimeException($errstr);
	}
	else {
		while (!feof($sock)) {
			$message = trim(fgets($sock));
		}
		fclose($sock);
	}
	$status = "ok";
}
catch (RuntimeException $e) {
	$message = $e->getMessage();
}

header('Content-Type: application/json');
echo json_encode(['status' => $status, 'message' => $message]);
?>
