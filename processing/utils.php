<?php
// require_once('RpcRequest.php');
// require_once('JsonRpcUnixDomainSocketClient.php');
require_once('class/JsonRequest.class.php');

function getMac() {
	return(strtolower(trim(exec('getmac'))));
}

/**
 * \fn getTitle()
 * \brief This function generate the web page title
 * \return The webpage title
 *
 * This function extract the hostname and the base MAC address for this device. It then create the title page using the name and the extracted MAC address separated by a '-'
 */
function getTitle() {
	return(trim(exec('hostname')) . ' - ' . getMac());
}

function getBrowserLang() {
	$lang = "";
	$supported_lang = array('fr', 'en', 'it', 'es', 'pt', 'ru');

	$languages = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
	$mainLang = explode('-', $languages[0]);
	$lang = $mainLang[0];

	if (!$lang || empty($lang) || !in_array($lang, $supported_lang)) {
		$lang = 'en';
	}

	return $lang;
}

function getBrowserCountry() {
	$country = "";
	$supported_country = array('FR', 'EN', 'IT', 'ES', 'PT', 'RU', 'US');

	$languages = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
	$mainLang = explode('-', $languages[0]);
	$country = $mainLang[1];

	if (!$country || empty($country) || !in_array($country, $supported_country)) {
		$country = 'FR';
	}

	return 	$country;
}

function setAnnouncingProtocolsStatus($status) {
	$dir = '../wizardData/data/';
	$file = scandir($dir, 1);
	$jsonSource = new JsonRequest('../wizardData/data/');

	if (isset($file[0]) && $file[0] != '.' && $file[0] != '..' && $file[0] != 'index.php') {
		$jsonData = $jsonSource->read($file[0]);
		$jsonData['ProtocolStatus'] = $status;
		$json = json_encode($jsonData);
		file_put_contents('../wizardData/data/' . $file[0], $json);
		return true;
	}
	elseif (isset($file[1]) && $file[1] != '.' && $file[1] != '..' && $file[1] != 'index.php') {
		$jsonData = $jsonSource->read($file[1]);
		$jsonData['ProtocolStatus'] = $status;
		$json = json_encode($jsonData);
		file_put_contents('../wizardData/data/' . $file[1], $json);
		return true;
	}
	else {
		return false;
	}
}

function setLanguage($lang) {
	$dir = '../wizardData/data/';
	$file = scandir($dir, 1);
	$jsonSource = new JsonRequest('../wizardData/data/');

	if (isset($file[0]) && $file[0] != '.' && $file[0] != '..' && $file[0] != 'index.php') {
		$jsonData = $jsonSource->read($file[0]);
		$jsonData['Language'] = $lang;
		$json = json_encode($jsonData);
		file_put_contents('../wizardData/data/' . $file[0], $json);
		return true;
	}
	elseif (isset($file[1]) && $file[1] != '.' && $file[1] != '..' && $file[1] != 'index.php') {
		$jsonData = $jsonSource->read($file[1]);
		$jsonData['Language'] = $lang;
		$json = json_encode($jsonData);
		file_put_contents('../wizardData/data/' . $file[1], $json);
		return true;
	}
	else {
		return false;
	}
}

function setWpsStatus($status) {
	$dir = '../wizardData/data/';
	$file = scandir($dir, 1);
	$jsonSource = new JsonRequest('../wizardData/data/');

	if (isset($file[0]) && $file[0] != '.' && $file[0] != '..' && $file[0] != 'index.php') {
		$jsonData = $jsonSource->read($file[0]);
		$jsonData['WpsStatus'] = $status;
		$json = json_encode($jsonData);
		file_put_contents('../wizardData/data/' . $file[0], $json);
		return true;
	}
	elseif (isset($file[1]) && $file[1] != '.' && $file[1] != '..' && $file[1] != 'index.php') {
		$jsonData = $jsonSource->read($file[1]);
		$jsonData['WpsStatus'] = $status;
		$json = json_encode($jsonData);
		file_put_contents('../wizardData/data/' . $file[1], $json);
		return true;
	}
	else {
		return false;
	}
}

// return the status of a directory, if exist or not
function directoryStatus($directory){
	$status = null;
  $filesFound = 0;
  if (is_dir($directory)) {
    if ($dh = opendir($directory)) {
      while (($file = readdir($dh)) !== false && $filesFound == 0) {
          // Ajout d'un bout de code pour travailler l'app sur un système mac
        if ($file != "." && $file != ".." && $file != 'index.php' && $file != '.DS_Store') {
				  $filesFound++;
			 	}
      }
      closedir($dh);
    }
  }

  if($filesFound == 0) {
		$status = 'notExist';
	}
  else {
		$status = 'Exist';
	}

	return $status;
}

// function for hash string or int
function hashPassword($password) {
	$options = [
		'cost' => 8,
	];
	$passHash = password_hash($password, PASSWORD_BCRYPT, $options);
	return $passHash;
}

function CIDRtoMask($int) {
	return long2ip(-1 << (32 - (int)$int));
}

function maskToCIDR($netmask) {
	$int = ip2long($netmask);
	$int = $int & 0xFFFFFFFF;
	$int = ( $int & 0x55555555 ) + ( ( $int >> 1 ) & 0x55555555 );
	$int = ( $int & 0x33333333 ) + ( ( $int >> 2 ) & 0x33333333 );
	$int = ( $int & 0x0F0F0F0F ) + ( ( $int >> 4 ) & 0x0F0F0F0F );
	$int = ( $int & 0x00FF00FF ) + ( ( $int >> 8 ) & 0x00FF00FF );
	$int = ( $int & 0x0000FFFF ) + ( ( $int >>16 ) & 0x0000FFFF );
	$int = $int & 0x0000003F;
	return $int;
}

?>
