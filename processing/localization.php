<?php
require_once('utils.php');

// Check if config file exist
$data = directoryStatus('./wizardData/data/');
if($data === 'Exist') {
  $dir = 'wizardData/data/';
  $file = scandir($dir, 1);
  $jsonSource = new JsonRequest('./wizardData/data/');
  if ($file[0] === "index.php") {
    $jsonData = $jsonSource->read($file[1]);
  }
  else {
    $jsonData = $jsonSource->read($file[0]);
  }
  // if exist, take language configured
  $lang_selected = $jsonData['Language'];
}
elseif (isset($_SESSION['lang'])) { // else if lang SESSION exist, take it
	$lang_selected = $_SESSION['lang'];
}
else {
	if (empty($lang_selected)) { // else, take browser language
		$lang_selected = getBrowserLang();
	}
}

$lang = "";
$locale = "";

// If configured language doesn't exist, get browser language


// Language analysis
switch($lang_selected) {
	case "fr":
		$lang = 'fr';
		$locale = 'fr_FR';
		break;
	case "en":
		$lang = 'en';
		$locale = 'en_GB';
		break;
	case "it":
		$lang = 'it';
		$locale = 'it_IT';
		break;
	case "es":
		$lang = 'es';
		$locale = 'es_ES';
		break;
	case "pt":
		$lang = 'pt';
		$locale = 'pt_PT';
		break;
	case "ru":
		$lang = 'ru';
		$locale = 'ru_RU';
		break;
	default:
		$lang = 'en';
		$locale = 'en_GB';
}

/********************* GETTEXT CONFIG ***************************/
error_reporting(E_ALL | E_STRICT);

// define constants
define('LOCALE_DIR', './internationnal/locale/');
define('DEFAULT_LOCALE', 'en_GB');

require_once('./lib/gettext/gettext.inc');

$supported_locales = array('en_GB', 'fr_FR', 'it_IT','es_ES', 'pt_PT', 'ru_RU');
$encoding = 'UTF-8';

// $locale = (isset($_GET['lang']))? $_GET['lang'] : DEFAULT_LOCALE;

// gettext setup
T_setlocale(LC_MESSAGES, $locale);
// Set the text domain as 'messages'
$domain = 'website';
bindtextdomain($domain, LOCALE_DIR);
// bind_textdomain_codeset is supported only in PHP 4.2.0+
if (function_exists('bind_textdomain_codeset'))
  bind_textdomain_codeset($domain, $encoding);
textdomain($domain);

?>
