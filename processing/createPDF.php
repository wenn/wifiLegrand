<?php
require_once('utils.php');
require_once('fpdf/fpdf.php');

$data = directoryStatus('../wizardData/data/');
if ($data == 'Exist') {

}
else {
	header('Location: index.php');
}

define('FPDF_FONTPATH','fpdf/font/');

class PDF extends FPDF {

	private $HREF = '';

	const A4_WIDTH = 210;

	function writeHTML($html) {
		// Parseur HTML
		$html = mb_convert_encoding($html, "ISO-8859-1", "UTF-8");

		$html = str_replace("\n",' ',$html);
		$a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
		foreach($a as $i=>$e) {
		 	if($i%2==0) {
				// Texte
				if($this->HREF)
 					$this->putLink($this->HREF,$e);
				else
					$this->Write(5,$e);
			}
			else {
				// Balise
				if($e[0]=='/')
					$this->closeTag(strtoupper(substr($e,1)));
				else {
					// Extraction des attributs
					$a2 = explode(' ',$e);
					$tag = strtoupper(array_shift($a2));
					$attr = array();
 					foreach($a2 as $v) {
						if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
							$attr[strtoupper($a3[1])] = $a3[2];
					}
					$this->openTag($tag,$attr);
				}
			}
		}
	}

	function putLink($URL, $txt) {
		// Place un hyperlien
		$this->SetTextColor(0,0,255);
		$this->setStyle('U',true);
		$this->Write(5,$txt,$URL);
		$this->setStyle('U',false);
		$this->SetTextColor(0);
	}

	function setStyle($tag, $enable) {
		// Modifie le style et sélectionne la police correspondante
		$this->$tag += ($enable ? 1 : -1);
		$style = '';
		foreach(array('B', 'I', 'U') as $s) {
			if($this->$s>0)
				$style .= $s;
		}
		$this->SetFont('',$style);
	}

	function openTag($tag, $attr) {
		// Balise ouvrante
		if($tag=='B' || $tag=='I' || $tag=='U')
			$this->setStyle($tag,true);
		if($tag=='A')
			$this->HREF = $attr['HREF'];
		if($tag=='BR')
			$this->Ln(5);
	}

	function closeTag($tag) {
		// Balise fermante
		if($tag=='B' || $tag=='I' || $tag=='U')
			$this->setStyle($tag,false);
		if($tag=='A')
			$this->HREF = '';
	}

	function centreImage($image, $image_height, $northing) {

		list($width, $height) = getimagesize($image);

		$scale = $image_height / $height;
		$image_width = $width * $scale;

		$easting = (self::A4_WIDTH - $image_width) / 2;

		$this->Image($image, $easting, $northing, $image_width, $image_height);
	}

	function writeConfig($config) {

		$config = mb_convert_encoding($config, "ISO-8859-1", "UTF-8");
		$this->SetFont('Arial', '', 14);
		$this->Cell(20);
		$this->MultiCell(0, 8, $config, 0, 'J');
	}

	function Header() {

		global $docTitle;

		$this->SetFont('Arial', 'B', 24);
		$this->MultiCell(0, 10, mb_convert_encoding($docTitle, "ISO-8859-1", "UTF-8"), 0, 'C');
	}
}

if( (isset($_GET['data']) && !empty($_GET['data'])) && (isset($_GET['inputHostname']) && !empty($_GET['inputHostname'])) ) {

	$title = $_GET['inputHostname'] . ' - ' /*. getMac()*/;
	$docTitle = "Configuration summary" . "\n" . $title;

	$pdf = new PDF();
	$pdf->SetTitle($title);
	$pdf->SetAuthor('Legrand');

	$pdf->AddPage("P");
	//$pdf->centreImage("images/ap_wifi.png", 60, 50);
	$pdf->SetY(130);
	$pdf->writeConfig($_GET['data']);
	$pdf->SetY(230);

	$html = "To open the administration interface of your access point, please click <a href=\"http://" . $_GET['inputHostname'] . ".local./\">here</a>";
	$pdf->writeHTML($html);

	$pdf->Output("../pdf/" . $title . ".pdf", 'F');

	echo json_encode(['status' => 'ok', 'pdf' => $title]);
}
else {
	echo "No configuration to display";
}
?>
