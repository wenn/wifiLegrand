		<?php
			include('include/inc_header.php');
			// Verif if a configuration exist, if exist redirect to home.php, else to identification.php
			require_once('processing/utils.php');
			try {
			  $data = directoryStatus('wizardData/data/');
			  if($data === 'Exist') {
			    header("Location: home.php");
			  }
			  elseif($data === 'notExist') {
			  	// stay on this page
			  }
			  else {
			    throw new Exception('An error has occured.');
			  }
			}
			catch(Exception $e) {
			  echo 'An error has occured';
			}
		?>
		<!-- Identification page CSS -->
		<link type="text/css" rel="stylesheet" href="css/identification.css">
	</head>

	<body>
 		<!-- Start forbidLandscape -->
		<div class="forbidLandscape hidden-sm hidden-lg hidden-md hidden">
			<div class="forbidLandscapeTable">
				<div class="forbidLandscapeTableCell">
					<p class="forbidLandscapeTableCellParaph text-center">
						<?= _('Veuillez utiliser votre smartphone en mode portrait'); ?>
					</p>
				</div>
			</div>
		</div> <!-- /forbidLandscape -->

		<form role="form" id="formCode" method="post" action="wizard.php" data-toggle="validator"> <!-- Formulaire -->
			<!-- Start Carousel -->
			<div class="identification" style="position: fixed">
				<!-- Wrapper for slides -->
				<div class="identification-inner">
					<!-- STEP 1 for Product Identification-->
					<div class="item">
						<!-- Container -->
						<div class="container-fluid">
							<!-- Header for Product Identification -->
							<div class="row header identif">
								<div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 hidden-xs margintop">
									<!-- Step title desktop version -->
									<h1 class="font-title"><?= _('Identification'); ?></h1>
								</div>
								<div class="hidden-lg hidden-md hidden-sm col-xs-12 margintop">
									<!-- Step title mobile version -->
									<h1 class="font-title majuscule"><?= _('Identification'); ?></h1>
								</div>
								<div class="col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-offset-1 col-sm-2 hidden-xs">
									<!-- Language Select -->
									<select class="flag-option font-title-select" id="langs">
										<option value="fr" id="fr">Français</option>
										<option value="en" id="en">English</option>
										<option value="it" id="it">Italiano</option>
										<option value="es" id="es">Español</option>
										<option value="pt" id="pt">Português</option>
										<option value="ru" id="ru">Русский</option>
									</select>
									<div class="alert alert-danger hidden" id="langError" role="alert"></div>
								</div>
							</div>
							<!--  Content of Container -->
							<div class="row content content-step-1">
								<div class="col-lg-offset-1 col-lg-11 col-md-offset-1 col-md-11 col-sm-offset-1 col-sm-11 col-xs-12 col-xs-offset-0">
									<div class="row">
										<!--Input form desktop version -->
										<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs form-element">
											<div class="form-group has-feedback group-feedback">
												<label for="inputCode" class="font-label justify"><?= _('Veuillez entrer le numéro se situant au dos de votre produit, comme indiqué sur le dessin ci-contre.'); ?></label>
												<br><br>
												<label class="font-label"><?= _('Identifiant du produit'); ?></label>
												<p class="font-label">
													<small><?= _('Suite de chiffres et de lettres de XX caractères.'); ?></small><br>
													<small class="hidden" id="CodeError" style="color:rgb(255,119,127)"></small>
												</p>
												<br>
												<input type="number" class="custom-input custom-input-field form-control" id="inputCode" name="inputCode" data-minlength="8" data-maxlength="8" maxlength="8" tabindex="3" value="" autofocus required>
												<!-- Contrôle du formulaire  -->
												<span class="glyphicon form-control-feedback pull-right" id="inputCodeSpan" aria-hidden="true"></span>
												 <div class="help-block with-errors"></div>
											</div>
											<!-- Button Next Desktop Version -->
											<div class="hidden-xs">
												<div class="f1-buttons pull-left">
												<input type="button"  id="wizard-validate-step1-link-lg-md" class="prev-next btn btn-submit" tabindex="4" value="<?= _('Suivant'); ?>">
												<!--{$wizard_suivant}-->
												</div>
											</div> <!-- /Button Desktop Version -->
										</div>
										<!-- Fin input field Desktop Version -->
										<!-- Input field Mobile Version -->
										<div class="hidden-lg hidden-md hidden-sm col-xs-12 form-element-mobile">
											<div class="form-group has-feedback has-success">
												<div class="font-label justify margintop"><!--{$wizard_cle}--><?= _('Veuillez entrer le numéro se situant au dos de votre produit, comme indiqué sur le dessin ci-contre.'); ?></div>
												<!-- Ajout de l'illustration en html pour la version mobile pour faciliter la position de l'image entre les paragraphes -->
												<div class="image-illustration margintop">
													<img class="img-responsive center-block" src="images/01_device_illustration.png" alt="Illustration du produit" width="250px" height="auto">
												</div>
												<label class="font-label margintop"><?= _('Identifiant du produit'); ?></label>
												<p class="font-label">
													<small><?= _('Suite de chiffres et de lettres de XX caractères.'); ?></small><br>
													<small class="hidden" id="CodeErrorMobile" style="color:rgb(255,119,127)"></small>
												</p>
												<br>
												<input type="text" class="custom-input input-text-mobile form-control custom-input-field" id="inputCodeMobile" name="inputCode" tabindex="3" data-minlength="8" maxlength="8" data-maxlength="8" value="" autofocus required>
												<span class="glyphicon form-control-feedback pull-right" id="inputCodeMobileSpan" aria-hidden="true"></span>
												 <p class="help-block with-errors"></p>
											</div>
										</div> <!-- /Fin input field Mobile Version -->
									</div>
								</div>
							</div> <!-- Fin row-content pour step1 -->
							<div class="row footer hidden-xs visible-lg visible-md visible-sm"> <!-- Début row footer pour step 1 et pour version desktop -->
		  					<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs margintop">
										<div class=" col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-offset-1 col-sm-2">
											<img class="img-responsive paddingtop" src="images/legrand_logo.png" alt="Logo Legrand">
										</div>
		  							<div class="col-lg-9 col-lg-offset-0 col-md-9 col-md-offset-0 col-sm-9 col-sm-offset-0"><!--version desktop Progress bar -->
		  								<div class="f1-steps">
		  									<div class="f1-step active">
		  										<div class="f1-step-icon">
		  											<img src="images/step01_device_ok_icon.png" alt="icon identification" class="block-center">
		  										</div>
		  										<p class="blanc button-active"><?= _('Identification'); ?></p>
		  									</div>
												<span id="bar2" class="progress_bar"></span>
		  									<div class="f1-step">
		  										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="f1-step-icon"><img src="images/step02_wifi_ko_icon.png" alt="icon Network"   class="block-center"></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  										<p class="button-inactive"><?= _('Réseau'); ?></p>
		  									</div>
		  									<span id="bar2" class="progress_bar"></span>
		  									<div class="f1-step">
		  										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="f1-step-icon"><img src="images/step03_acess_point_ko_icon.png" alt="icon access point"  class="block-center"></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  										<p class="button-inactive"><?= _('Point d\'accès'); ?></p>
		  									</div>
		  								</div>	<!-- Fin Progress bar version desktop -->
		  							</div>
										<div class="clearfix"></div>
		  						</div>
		  					</div>
		  				</div>
							<div class="row footer hidden-lg hidden-md hidden-sm visible-xs"> <!-- Début row footer pour step 1 et pour version mobile -->
							<!-- Button Mobile Version -->
							<div class="row hidden-lg hidden-md hidden-sm visible-xs margintop">
								<div class="col-xs-offset-1 col-xs-10">
									<input type="button"  id="wizard-validate-step1-link-mobile" class="prev-next btn btn-submit btn-block" tabindex="4" value="<?= _('Suivant'); ?>">
								</div> <!-- /Button Mobile Version -->
							</div>


									<div class="progress" style="position:absolute; bottom:0;left:0;width:100%;">
										<!-- Progress bar version mobile -->
										<div class="progress-bar progress-bar-pink" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:33.33%">
											<span class="sr-only"><?= _('30% effectué'); ?></span>
										</div>
									</div> <!-- Fin barre de progression -->

							</div>
						</div> <!-- Fin container fluid -->
					</div> <!-- /item-->
				</div> <!-- Fin identification inner -->
			</div>	<!-- End Carousel -->
		</form>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="js/jquery.touchSwipe.min.js"></script>
		<script src="js/ipValidation.js"></script>
		<!-- Validator form Bootstrap côté front -->
		<script src="js/validator.js"></script>

		<script type="text/javascript">
			// var initial_screen_size;
			// var is_keyboard = false;
			// var viewport;
	    //
			// function findBootstrapEnvironment() {
			// 		var envs = ["ExtraSmall", "Small", "Medium", "Large"];
			// 		var envValues = ["xs", "sm", "md", "lg"];
	    //
			// 		var $el = $('<div>');
			// 		$el.appendTo($('body'));
	    //
			// 		for (var i = envValues.length - 1; i >= 0; i--) {
			// 	var envVal = envValues[i];
	    //
			// 	$el.addClass('hidden-'+envVal);
			// 	if ($el.is(':hidden')) {
			// 			$el.remove();
			// 			return envs[i]
			// 	}
			// 		};
			// }
	    //
			// function updateViews() {
			// 		is_keyboard = (window.innerHeight < initial_screen_size);
			// 		if (is_keyboard) {
			// 	$(".footer-2").addClass("hidden");
			// 		}
			// 		else {
			// 	$(".footer-2").removeClass("hidden");
			// 		}
			// }
	    //
			// function doOnOrientationChange() {
			// 	switch(window.orientation) {
			// 		case -90:
			// 		case 90:
			// 			$(".forbidLandscape").removeClass("hidden");
			// 			break;
			// 		default:
			// 			$(".forbidLandscape").addClass("hidden");
			// 			break;
			// 	}
			// }
	    //
			// viewport = findBootstrapEnvironment();
	    //
			// $(document).ready(function() {
			// 	$('.carousel').carousel({
			// 			interval: false
			// 	});
	    //
			// 	if ( (viewport == 'ExtraSmall') || (viewport == 'Small')) {
	    //
			// 		//Enable swiping...
			// 		$(".carousel-inner").swipe( {
			// 			//Generic swipe handler for all directions
			// 			swipeLeft:function(event, direction, distance, duration, fingerCount) {
			// 				if ( ($('.carousel-inner .item:first').hasClass('active')) && ($('#inputLang option:selected').val() != '') ) {
			// 					$(this).parent().carousel('next');
			// 					$('#inputCode').focus();
			// 				}
			// 			},
			// 			swipeRight: function() {
			// 				if (!$('.carousel-inner .item:first').hasClass('active')) {
			// 					$(this).parent().carousel('prev');
			// 					$('#inputLang').focus();
			// 				}
			// 			},
			// 			//Default is 75px, set to 0 for demo so any distance triggers swipe
			// 			threshold:0
			// 		});
			// 	}
	    //
			// 	$('#inputLang').focus();
	    //
			// 	if ($('#inputLang option:selected').val() != '') {
			// 		$('#connexion-next-link-lg-md, #connexion-next-link-sm-xs').removeClass("disabled");
			// 		$('#connexion-next-img-lg-md, #connexion-next-img-sm-xs').prop('src', 'images/next_icon_on.png');
			// 	}
	    //
			// 	if( $('#inputCode').val().length == 8 ) {
			// 		$("button[name='inputValidate-lg-md'], a[name='inputValidate-sm-xs']").removeClass("disabled");
			// 	}
	    //
			// 	initial_screen_size = window.innerHeight;
			// 	doOnOrientationChange();
			// });
	    //
			// /**
			//  * Gestion de l'orientation
			// **/
			// window.addEventListener('orientationchange', doOnOrientationChange);
	    //
			// /**
			//  * Gestion clavier virtuel
			// **/
	    //
			// /* Android */
			// window.addEventListener('resize', updateViews);
	    //
			// /* iOS */
			// $('#inputCode').bind("focus blur", function() {
			// 	$(window).scrollTop(10);
			// 	is_keyboard = $(window).scrollTop() > 0;
			// 	$(window).scrollTop(0);
			// 	updateViews();
			// });
	    //
			// /**
			//  * Navigation
			// **/
			// $('#connexion-prev-link-lg-md, #connexion-prev-link-sm-xs').on({
			// 	click: function() {
			// 		$('#carousel-connexion').carousel('prev');
			// 		$('#inputLang').focus();
			// 	}
			// });
	    //
			// $('#connexion-next-link-lg-md, #connexion-next-link-sm-xs').on({
			// 	click: function() {
			// 		$('#carousel-connexion').carousel('next');
			// 		$('#inputCode').focus();
			// 	},
			// 	keydown: function(event) {
			// 		if (event.which == 13 || event.keyCode == 13) {
			// 			if ($('#inputLang option:selected').val() == '') {
			// 				event.preventDefault();
			// 			}
			// 		}
			// 	}
			// });

	    // Recovery language in (php)$_SESSION and attribute to #langs
	    var langSession = "<?php if(isset($_SESSION['lang'])){echo $_SESSION['lang'];}?>";
	    var langBrowser = "<?php if(isset($lang)){echo $lang;}?>";
	    if(langSession.length > 0) {
	      var realLang = langSession;
	    }
	    else {
	      var realLang = langBrowser;
	    }
	    $("#langs option[value='" + realLang + "']").attr('selected', 'selected');

			/**
			 * Change Language
			 **/
			$('#langs').on('change', function() {
				if(this.value != '') {
					$.ajax({
						url: "processing/i18n-identification-cgi.php",
						type: "post",
						data: "lang=" + $("#langs option:selected").val(),
						dataType: 'json',
						success: function(json) {
							if (json.reponse === "ok") {
								location.reload(true);
								console.log(json.message);
							}
							else if (json.reponse === "error") {
								console.log(json.message);
							}
						},
						fail: function() {
							console.log("error on server");
						}
					});
				}
			});

			/**
			 * DESKTOP VERSION ONLY
			 * Enable button if exist 8 characters in #inputCode
			 * Else it's disable
			 * AND
			 * Submit the form when Enter Key is pressed
			 **/
			 $('#inputCode').on({
			 	keyup: function() {
			 		if( $(this).val().length == 8 ) {
			 			$("#wizard-validate-step1-link-lg-md").removeClass("disabled");
			 		}
			 		else {
			 			$("#wizard-validate-step1-link-lg-md").addClass("disabled");
			 		}
			 	},
			 	keypress: function(event) {
			 		if (event.which == 13 || event.keyCode == 13) {
			 			$("#wizard-validate-step1-link-lg-md").click();
			 			event.preventDefault();
			 		}
			 	}
			 });

			/**
			 * MOBILE VERSION ONLY
			 * Enable button if exist 8 characters in #inputCode
			 * Else it's disable
			 * AND
			 * Submit the form when Enter Key is pressed
			 **/
			 $('#inputCodeMobile').on({
			 	keyup: function() {
			 		if( $(this).val().length == 8 ) {
			 			$("#wizard-validate-step1-link-mobile").removeClass("disabled");
			 		}
			 		else {
			 			$("#wizard-validate-step1-link-mobile").addClass("disabled");
			 		}
			 	},
			 	keypress: function(event) {
			 		if (event.which == 13 || event.keyCode == 13) {
			 			$("#wizard-validate-step1-link-mobile").click();
			 			event.preventDefault();
			 		}
			 	}
			 });

			/**
			 * Clic sur Valider
			 * DESKTOP VERSION
			 **/
			 $("#wizard-validate-step1-link-lg-md").click(function() {
			 	$.ajax({
			 		url: 'processing/connexion-cgi.php',
			 		type: "post",
			 		data: "code=" + $("#inputCode").val() + "&lang=" + $("#langs option:selected").val(),
			 		dataType: 'json',
			 		success: function(json) {
			 			console.log(json);
			 			if(json.reponse == 'ok') {
			 				// $("#formCode").submit();
			 				$(window).attr("location", './wizard.php');
			 			}
			 			else {
			 				$("#CodeError").removeClass("hidden");
			 				$("#CodeError").text(json.reponse);
			 			}
			 		},
			 		fail: function() {
			 			console.log("error on server");
			 		}
			 	});
			 });

			/**
			 * Clic sur Valider
			 * MOBILE VERSION
			 **/
			 $("#wizard-validate-step1-link-mobile").click(function() {
			 	$.ajax({
			 		url: 'processing/connexion-cgi.php',
			 		type: "post",
			 		data: "code=" + $("#inputCodeMobile").val(),
			 		dataType: 'json',
			 		success: function(json) {
			 			console.log(json);
			 			if(json.reponse == 'ok') {
			 				// $("#formCode").submit();
			 				$(window).attr("location", './wizard.php');
			 			}
			 			else {
			 				$("#CodeErrorMobile").removeClass("hidden");
			 				$("#CodeErrorMobile").text(json.reponse);
			 			}
			 		},
			 		fail: function() {
			 			console.log("error on server");
			 		}
			 	});
			 });
		</script>
	</body>
	</html>
