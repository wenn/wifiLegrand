<?php
// Verif if a configuration exist, if exist redirect to home.php, else to identification.php
require_once('processing/utils.php');
try {
  $data = directoryStatus('wizardData/data/');
  if($data === 'Exist') {
    require_once('home.php');
  }
  elseif($data === 'notExist') {
    require_once('identification.php');
  }
  else {
    throw new Exception('An error has occured.');
  }
}
catch(Exception $e) {
  echo 'An error has occured';
}
?>
