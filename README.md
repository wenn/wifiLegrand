######################## Mini Guide For WebApp ########################

Root files   = 	All html page.

In folder css/  =  Stylesheet files.

In folder images/   =   All img needed for the webapp.

In folder include/   =   Head for differents pages of webapp. 

In folder internationnal/   =   GetText lib.

In folder js/    =   Js lib.

In folder lib/   =   getText lib.

In folder pdf/   =   .pdf files saved here.

In folder processing/    =   All (php) files are needed for processing.
      ->   Folder fpdf   =   pdf lib.
      ->   Folder class  =   class.php used for read and create files.

In folder WizardData/data/   =   Configuration of access point saved here.

########################################################################

Exec code (ex: exec('wpa_passphrase')) are in : wizard-cgi.php, connexion-cgi.php, utils.php, createPDF.php.

Localization.php == GetText initialization.

##### IF YOU'RE REDIRECTED TO Home.php WHEN YOU ACCESS TO Index.php, DELETE THE FILE (not index.php) IN WizardData/data/xxxxxxx FOR RESET THE WIZARD #####