<?php
session_start();
require_once('processing/localization.php');

try {
 $data = directoryStatus('./wizardData/data/');
  if($data === 'Exist') {
    $dir = 'wizardData/data/';
    $file = scandir($dir, 1);
    $jsonSource = new JsonRequest("./wizardData/data/");
    if ($file[0] === "index.php") {
      $jsonData = $jsonSource->read($file[1]);
    }
    else {
      $jsonData = $jsonSource->read($file[0]);
    }
    $lang = $jsonData['Language'];

    if (!isset($_SESSION['lang'])) {
      $_SESSION['lang'] = $jsonData['Language'];
    }

    // Variable for checkbox
    $checked = 'checked';

    //Variable for date
    if($lang == 'fr') {
      ($jsonData['Date']['month'] == 'January') ? $jsonData['Date']['month'] = "Janvier" : '' ;
      ($jsonData['Date']['month'] == 'February') ? $jsonData['Date']['month'] = "Février" : '' ;
      ($jsonData['Date']['month'] == 'March') ? $jsonData['Date']['month'] = "Mars" : '' ;
      ($jsonData['Date']['month'] == 'April') ? $jsonData['Date']['month'] = "Avril" : '' ;
      ($jsonData['Date']['month'] == 'May') ? $jsonData['Date']['month'] = "Mai" : '' ;
      ($jsonData['Date']['month'] == 'June') ? $jsonData['Date']['month'] = "Juin" : '' ;
      ($jsonData['Date']['month'] == 'July') ? $jsonData['Date']['month'] = "Juillet" : '' ;
      ($jsonData['Date']['month'] == 'August') ? $jsonData['Date']['month'] = "Aout" : '' ;
      ($jsonData['Date']['month'] == 'September') ? $jsonData['Date']['month'] = "Septembre" : '' ;
      ($jsonData['Date']['month'] == 'October') ? $jsonData['Date']['month'] = "Octobre" : '' ;
      ($jsonData['Date']['month'] == 'November') ? $jsonData['Date']['month'] = "Novembre" : '' ;
      ($jsonData['Date']['month'] == 'December') ? $jsonData['Date']['month'] = "Décembre" : '' ;
      $date = $jsonData['Date']['day'] . ' ' . $jsonData['Date']['month'] . ' ' . $jsonData['Date']['year'];
    }
    else {
      ($jsonData['Date']['month'] == 'Janvier') ? $jsonData['Date']['month'] = "January" : '' ;
      ($jsonData['Date']['month'] == 'Février') ? $jsonData['Date']['month'] = "February" : '' ;
      ($jsonData['Date']['month'] == 'Mars') ? $jsonData['Date']['month'] = "March" : '' ;
      ($jsonData['Date']['month'] == 'Avril') ? $jsonData['Date']['month'] = "April" : '' ;
      ($jsonData['Date']['month'] == 'Mai') ? $jsonData['Date']['month'] = "May" : '' ;
      ($jsonData['Date']['month'] == 'Juin') ? $jsonData['Date']['month'] = "June" : '' ;
      ($jsonData['Date']['month'] == 'Juillet') ? $jsonData['Date']['month'] = "July" : '' ;
      ($jsonData['Date']['month'] == 'Aout') ? $jsonData['Date']['month'] = "August" : '' ;
      ($jsonData['Date']['month'] == 'Septembre') ? $jsonData['Date']['month'] = "September" : '' ;
      ($jsonData['Date']['month'] == 'Octobre') ? $jsonData['Date']['month'] = "October" : '' ;
      ($jsonData['Date']['month'] == 'Novembre') ? $jsonData['Date']['month'] = "November" : '' ;
      ($jsonData['Date']['month'] == 'Décembre') ? $jsonData['Date']['month'] = "December" : '' ;
      $date = $jsonData['Date']['year'] . ' ' . $jsonData['Date']['month'] . ' ' . $jsonData['Date']['day'];
    }
  }
  elseif($data === 'notExist') {
    header("Location: index.php");
  }
  else {
    throw new Exception('An error has occured.');
  }
}
catch (Exception $e) {
  $e->getMessage('An error has occured');
}
?>
<!DOCTYPE html>
<html lang=<?php if(isset($lang)){echo '"' . $lang . '"';} else {echo '"fr"';} ?>>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="Nicolas Gillen <nicolas.gillen@legrand.fr>">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/wifi-soho.css">
    <link type="text/css" rel="stylesheet" href="css/help-class.css">
    <link rel="icon" href="favicon.ico">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
