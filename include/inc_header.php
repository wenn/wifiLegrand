<?php
session_start();
require_once('./processing/localization.php');
?>
<!DOCTYPE html>
<html lang=<?php if(isset($lang)){echo '"' . $lang . '"';} else {echo '"fr"';} ?>>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="Nicolas Gillen <nicolas.gillen@legrand.fr>">
      <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/wifi-soho.css">
    <link type="text/css" rel="stylesheet" href="css/help-class.css">
    <link rel="icon" href="favicon.ico">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <!-- [if (It IE 9) & (!IEMobile)]>
      <script src="../js/css3-mediaqueries.js"></script>
    <![endif]-->
